$(function(){
	$('.user-menu-list').find('li').hover(function(){
		$(this).find('.user-info-tow').toggle();
	});
});
/**
 * [sendGet 发送有提示的GET]
 * @param  {[type]} path  [description]
 * @param  {[type]} title [description]
 * @return {[type]}       [description]
 */
function sendGet(path,title){
	var path = path ? path : '';
	var title = title ? title : '';
	if (path == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	if (title == '') {
		layer.msg('请传入GET标题名称');
		return;
	};
	layer.confirm(title,function(){
		$.getJSON(path,function(msg){
			if (msg.status == 1) {
				layer.msg(msg.info,function(){
					window.location.href=msg.url;
				});
			}else{
				layer.msg(msg.info);
			};
		});
	});
}
/**
 * [GetUrl 请求目标URL]
 * @param {[type]} url [description]
 */
function GetUrl(url){
	layer_index = layer.msg('请稍等，正在加载中...',{
		icon : 16,
		time : 30 * 1000,
	});
	window.location.href=url;
}
/**
 * [send_none_Get 发送无提示GET参数]
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
function send_none_Get(path){
	var path = path ? path : '';
	if (path == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	$.getJSON(path,function(msg){
		if (msg.status == 1) {
			layer.msg(msg.info,function(){
				window.location.href = msg.url;
			});
		}else{
			layer.msg(msg.info);
		};
	});
}
/**
 * [url_location 提示后跳转]
 * @param  {[type]} url   [description]
 * @param  {[type]} title [description]
 * @return {[type]}       [description]
 */
function url_location(url,title){
	var url = url ? url : '';
	var title = title ? title : '';
	if (url == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	if (title == '') {
		layer.msg('请传入GET标题名称');
		return;
	};
	layer.confirm(title,function(){
		window.location.href=url;
	});
}
/**
 * [login_box description]
 * @return {[type]} [description]
 */
function login_box(url, width, height){
	var width = '430px';
	var height = '250px';
	if (!url) {
		layer.msg('请传入请求地址');
		return false;
	};
	layer.open({
		title: false,
		type: 2,
		skin: 'yourclass',
		area: [width, height],
		fix: false, //不固定
		maxmin: false,
		shadeClose: true,
		closeBtn: 0,
		content: url,
	});
}
/**
 * [isURL 验证是否URL]
 * @param  {[type]}  url [http网址]
 * @return {Boolean}     [description]
 */
function isURL(url) {
	var strRegex = "^((https|http|ftp|rtsp|mms)://)?[a-z0-9A-Z]{3}\.[a-z0-9A-Z][a-z0-9A-Z]{0,61}?[a-z0-9A-Z]\.com|net|cn|cc (:s[0-9]{1-4})?/$";
	var re = new RegExp(strRegex);
	if (re.test(url)) {
		return true;
	} else {
		return false;
	}
}
/**
 * [is_mobile 验证合法的手机号码]
 * @param  {[type]}  aPhone [description]
 * @return {Boolean}        [description]
 */
function is_mobile(aPhone) {
	var bValidate = RegExp(/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/).test(aPhone);
	if (bValidate) {
		return true;
	}else{
		return false;
	}
}