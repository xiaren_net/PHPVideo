$(function(){
	var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'pickfiles',
        uptoken_url: GetTokenJSON,
        domain: QiniuDomain,
        // unique_names: true,
        get_new_uptoken: false,  //设置上传文件的时候是否每次都重新获取新的token
        container: 'container',           //上传区域DOM ID，默认是browser_button的父元素
        flash_swf_url: PUBLIC_PATH+'/Plupload/Moxie.swf',  //引入flash,相对路径
        max_retries: 3,                   //上传失败最大重试次数
        dragdrop: true,                   //开启可拖曳上传
        get_new_uptoken: false,           // 获取最新TOKEN
        multi_selection : false,		  // 每次只能选择一个文件
        drop_element: 'container',        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
        chunk_size: '5mb',                //分块上传时，每片的体积
        auto_start: true,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传
        filters : {
        	max_file_size : '2000mb',
        	prevent_duplicates: true,
        	mime_types: [
        		{title : "请选择正确MP4格式视频或者FLV视频格式进行上传", extensions : "AVI,avi,FLV,flv,mp4,MP4,wmv,WMV"}
        	]
        },
        init: {
            'FilesAdded': function(up, files) {
            	$('#container').hide();
                plupload.each(files, function(file) {
                    var progress = new FileProgress(file, 'fsUploadProgress');
                    progress.setStatus("等待...");
                });
            },
            'BeforeUpload': function(up, file) {
                // 每个文件上传前,处理相关的事情
                var progress = new FileProgress(file, 'fsUploadProgress');
                var chunk_size = plupload.parseSize(this.getOption('chunk_size'));
                if (up.runtime === 'html5' && chunk_size) {
                    progress.setChunkProgess(chunk_size);
                }
            },
            'UploadProgress': function(up, file) {
                // 每个文件上传时,处理相关的事情
                var progress = new FileProgress(file, 'fsUploadProgress');
                var chunk_size = plupload.parseSize(this.getOption('chunk_size'));
                progress.setProgress(file.percent + "%", file.speed, chunk_size);
            },
            'FileUploaded': function(up, file, info) {
                   // 每个文件上传成功后,处理相关的事情
                   // 其中 info 是文件上传成功后，服务端返回的json，形式如
                   // {
                   //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                   //    "key": "gogopher.jpg"
                   //  }
                   // 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html

                   // var domain = up.getOption('domain');
                   // var res = parseJSON(info);
                   // var sourceLink = domain + res.key; 获取上传成功后的文件的Url
                   var progress = new FileProgress(file, 'fsUploadProgress');
                   progress.setComplete(up, info);
                   var obj = jQuery.parseJSON(info);
                   var is_key = obj.key ? obj.key : 'no';
                   window.location.href= VideoEditUrl+"?hash="+obj.hash+"&key="+is_key+"&info_name="+file.name;
            },
            'Error': function(up, err, errTip) {
            	//上传出错时,处理相关的事情
            	$('table').show();
            	var progress = new FileProgress(err.file, 'fsUploadProgress');
            	progress.setError();
            	progress.setStatus(errTip);
            },
            'UploadComplete': function(up,file) {
            	//队列文件处理完毕后,处理相关的事情
            },
            'Key': function(up, file) {
                // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
                // 该配置必须要在 unique_names: false , save_key: false 时才生效

                var key = "";
                // do something with key here
                return key
            }
        },
    });
});