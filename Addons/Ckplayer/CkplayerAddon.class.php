<?php
namespace Addons\Ckplayer;
use Common\Controller\Addon;
/**
 * ckplayer视频播放器插件
 * @author 楚羽幽
 */
class CkplayerAddon extends Addon{
    public $info = array(
        'name'=>'Ckplayer',
        'title'=>'ckplayer视频播放器插件',
        'description'=>'Ckplayer视频播放器插件6.8',
        'status'=>1,
        'author'=>'楚羽幽',
        'version'=>'0.1'
    );
    
    // 安装插件
    public function install(){
        return true;
    }

    // 卸载插件
    public function uninstall(){
        return true;
    }

    //实现的VideoDetail钩子方法
    public function VideoDetail($param){
        $this->assign('addon_path', $param['MIC_DOMAIN'] . '/' . $this->addon_path);
        $this->assign('param', $param);
        $this->display('ckplayer');
    }
}