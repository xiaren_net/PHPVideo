<?php
$result = array(
	'VIEW_PATH'				=> 'Theme/PC/',	//视图模版路径
	
	/****************************错误提示模板**************************************/
    'TMPL_ACTION_ERROR'     =>  'Theme/PC/error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  'Theme/PC/success.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  'Theme/PC/exception.html',// 异常页面的模板文件
);
return array_merge($result,require 'Data/Config/Theme.config.php');