<?php
/**
 * [home_is_login 检测前台用户是否登录]
 * @return [type]
 */
function home_is_login(){
	if (empty($_SESSION['Home']['uid'])) {
		return false;
	}else{
		return true;
	}
}
/**
 * [GetUserInfo 获取用户数据]
 */
function GetUserInfo(){
	$user_id = $_SESSION['Home']['uid'];
	if (empty($user_id)) {
		return;
	}else{
		$user_info = M('Member')->where(array('user_id'=> $user_id))->find();
		return $user_info;
	}
}
/**
 * [GetTokenKey 获取加密后的KEY]
 */
function GetTokenKey(){
	$key_time = S('KeyTime');
	if (empty($key_time)) {
		S('KeyTime',array('time'=> time() + 60, 'token'=> md5(C('MIC_TOKEN_KEY') . mt_rand(1,100))));
	}else{
		if ($key_time['time'] < time()) {
			S('KeyTime',array('time'=> time() + 60, 'token'=> md5(C('MIC_TOKEN_KEY') . mt_rand(1,100))));
			return $key_time['token'];
		}else{
			return $key_time['token'];
		}
	}
}