<?php
namespace Home\Controller;
use Think\Controller;
class BaseController extends Controller {
	// 构造函数
    public function _initialize(){
        // 判断程序是否已安装
        if (!in_array(MODULE_NAME, 'Install') && !file_exists('Data/Install.lock')) {
            header('location:index.php?m=Install');
        }
        // 设置模版常量
    	self::MIC_WEB_PATH();

        $this->user_info = GetUserInfo();

        // 分配数据
        $this->seo_title = '微电影,网络微电影,好看的微电影,原创微电影';
        $this->web_name = '奇亦微视网';
        $this->mic_keywords = '微电影,网络微电影,好看的微电影,原创微电影';
        $this->mic_description = '描述';
    }

    /**
     * [GetTokenJSON 获取七牛JSON数据Token]
     */
    public function GetTokenJSON(){
        $TokenKey = $_GET['TokenKey'];
        if (empty($TokenKey)) {
            die('非法请求TOKEN数据');
        }
        if ($TokenKey != GetTokenKey()) {
            $this->redirect('Home/Videos/add');
        }
        $token_path = 'Data/QiniuToken.lock';
        if (!file_exists($token_path)) {
            $token = GetQiniuToken($token_path);
            exit(json_encode(array('uptoken'=> $token)));
        }else{
            $token = json_decode(file_get_contents($token_path));
            if ($token->time < time()) {
                $token = GetQiniuToken($token_path);
                exit(json_encode(array('uptoken'=> $token)));
            }else{
                exit(json_encode(array('uptoken'=> $token->token)));
            }
        }
    }
    
    /**
     * [MIC_WEB_PATH 设置页面常量]
     */
    static public function MIC_WEB_PATH(){
        defined('__THEME__') ? __THEME__ : define('__THEME__', __ROOT__ . '/Theme/PC');
        defined('__RES__') ? __RES__ : define('__RES__', __THEME__ . '/' . C('DEFAULT_THEME') . '/res');
        defined('__CSS__') ? __CSS__ : define('__CSS__', __RES__ . '/css');
        defined('__IMAGES__') ? __IMAGES__ : define('__IMAGES__', __RES__ . '/images');
        defined('__JS__') ? __JS__ : define('__JS__', __RES__ . '/js');
    }
}