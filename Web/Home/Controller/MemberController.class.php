<?php
namespace Home\Controller;
class MemberController extends BaseController {
	// 构造函数
	public function _initialize(){
		parent::_initialize();
		// 访问操作方法权限
		$_Controller_action = array(
			'Videos/info'
		);
		if (empty($_SESSION['Home']['uid']) && !in_array(CONTROLLER_NAME . '/' .ACTION_NAME, $_Controller_action)) {
			$this->redirect('Home/Public/login');
		}

		// 用户UID
		$this->user_uid = $_SESSION['Home']['uid'];
	}
	// 用户中心首页
    public function index(){
        $this->display();
    }

    /**
     * [myinfo 用户信息]
     * @return [type] [description]
     */
    public function myinfo(){
    	$this->display();
    }
}