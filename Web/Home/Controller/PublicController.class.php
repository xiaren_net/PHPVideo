<?php
namespace Home\Controller;
class PublicController extends BaseController {
    // 构造函数
	public function _initialize(){
		parent::_initialize();
		// 访问规则定义
		$action_rule = array(
            'user_out'
		);
		// 判断已登录跳转
		if (!in_array(ACTION_NAME, $action_rule)) {
			if (home_is_login()) {
				$this->error('你已经登录，正在跳转', U('Home/Member/index'));
			}
		}
		$this->db = D('Member');
	}
	/**
	 * [login 用户登录]
	 * @return [type]
	 */
    public function login(){
    	if (IS_POST) {
    		if ($this->db->login()) {
    			$this->success('登录成功',U('Home/Member/index'));
    		}else{
    			$this->error($this->db->getError());
    		}
    	}else{
    		$this->display();
    	}
    }
    /**
     * [register 用户注册]
     * @return [type]
     */
    public function register(){
    	if (IS_POST) {
            if ($this->db->register()) {
                $this->success('注册成功',U('Home/Public/login'));
            }else{
                $this->error($this->db->getError());
            }
    	}else{
    		$this->display();
    	}
    }
    /**
     * [user_out 退出登录]
     * @return [type] [description]
     */
    public function user_out(){
        unset($_SESSION['Home']['uid']);
        $this->success('退出成功',U('Index/index'));
    }
}