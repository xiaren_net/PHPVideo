<?php
namespace Home\Controller;
class IndexController extends BaseController {
	// 构造函数
	public function _initialize(){
		parent::_initialize();
	}
	// 首页
    public function index(){
        $this->display();
    }

    /**
     * [search 搜索视频]
     * @return [type] [description]
     */
    public function search(){
    	$keywords = $_GET['keywords'];
    	if (empty($keywords)) {
    		$this->error('请输入视频关键词',U('Home/Index/index'));
    	}
    	$video_info = D('Videos')->relation(true)->where(array('status'=> 1,'video_title'=> array('LIKE',"%{$keywords}%")))->select();
    	foreach ($video_info as $key => $ret) {
    		$video_info[$key]['video_title'] = str_replace($keywords,"<span class='text-red'>{$keywords}</span>",$ret['video_title']);
    	}
    	$this->assign('serarch_data', $video_info);
    	$this->display();
    }
}