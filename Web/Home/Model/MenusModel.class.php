<?php
namespace Home\Model;
use Think\Model;
class MenusModel extends Model{

	/**
	 * [GetMenusList 获取菜单数据标签]
	 * @param [type] $row   [获取数量]
	 * @param [type] $order [排序方式]
	 */
	public function GetMenusList($row, $order){
		// 组装排序
		$order = str_replace(',', ' ', $order);
		$order = str_replace('|', ',', $order);
		// 获取数据
		$data = $this->where(array('status'=> 1))->limit($row)->order($order)->select();
		// 组装数据
		foreach ($data as $key => $ret) {
			$data[$key]['url'] = strstr($ret['url'], 'http://') ? $ret['url'] : C('MIC_DOMAIN') . U($ret['url']);
		}
		$data = \Common\Util\Data::channelLevel($data, 0, '&nbsp;', 'id');
		return $data;
	}
}