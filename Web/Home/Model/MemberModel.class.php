<?php
namespace Home\Model;
use Think\Model;
class MemberModel extends Model{
	// 自动验证
	protected $_validate = array(
		array('mobile','','手机号码已存在，请重新尝试',0,'unique',1),
		array('nick_name','','用户昵称已存在，请重新尝试',0,'unique',1),
	);

	// 自动完成
	protected $_auto = array(
		array('create_time','time',1,'function'),
		array('user_pass','_encrypt',3,'function'),
	);
	/**
	 * [login 用户登录]
	 * @return [type] [description]
	 */
	public function login(){
		$mobile = $_POST['mobile'];
		if (!$user_info = $this->where(array('mobile'=> $mobile))->find()) {
			$this->error = '用户不存在';
			return false;
		}
		if ($user_info['user_pass'] != _encrypt($_POST['user_pass'])) {
			$this->error = '登录密码错误';
			return false;
		}
		$_SESSION['Home']['uid'] = $user_info['user_id'];
		return true;
	}

	/**
	 * [register 用户注册]
	 * @return [type] [description]
	 */
	public function register(){
		if ($this->create()) {
			if ($uid = $this->add()) {
				return $uid;
			}else{
				$this->error = '非法错误，注册失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}
}