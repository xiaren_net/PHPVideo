<?php
namespace Home\Model;
use Think\Model\RelationModel;
class VideosModel extends RelationModel{
	// 自动完成
	protected $_auto = array(
		array('create_time', 'time', 1, 'function'),
	);
	// 数据关联
	protected $_link = array(
        'Member'	=>	array(
        	'mapping_type'	=> self::BELONGS_TO,
        	'class_name'	=> 'Member',
        	'foreign_key'	=> 'video_uid',
        	'mapping_fields'=> 'user_id,mobile,nick_name,avatar,create_time',
        ),
        'Category'	=>	array(
        	'mapping_type'	=> self::BELONGS_TO,
        	'class_name'	=> 'Category',
        	'foreign_key'	=> 'video_cid',
        	// 'mapping_fields'=> '',
        ),
    );
	/**
	 * [VideoSave 编辑视频内容]
	 */
	public function VideoSave(){
		if ($this->create()) {
			$status = $this->where(array('video_id'=> $_POST['id']))->save();
			if ($status !== false) {
				return true;
			}else{
				$this->error = '视频编辑失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [GetVideoKey 获取单视频KEY数据]
	 * @param [type] $video_info [description]
	 */
	public function GetVideoKey($video_info){
		if (empty($video_info['video_sd_key']) && empty($video_info['video_high_key']) && empty($video_info['video_super_key'])) {
			// 标清
			$video_sd_key = VideoKeyStatus($video_info['video_sd_id'], 'player');
			$video_data_save['video_sd_key'] = $video_sd_key['items'][0]['key'];
			// 高清
			$video_high_key = VideoKeyStatus($video_info['video_high_id'], 'player');
			$video_data_save['video_high_key'] = $video_high_key['items'][0]['key'];
			// 超清
			$video_super_key = VideoKeyStatus($video_info['video_super_id'], 'player');
			$video_data_save['video_super_key'] = $video_super_key['items'][0]['key'];
			// 批量更新
			foreach ($video_data_save as $key => $ret) {
				$this->where(array('video_id'=> $video_info['video_id']))->save(array($key=> $ret));
			}
			// 判断高清KEY
			if (!empty($video_info['video_high_key'])) {
				$video_info['default_key'] = $video_info['video_high_key'];
				return $video_info;
			}else{
				$video_info['default_key'] = $video_info['video_key'];
				return $video_info;
			}
		}else{
			$video_info['default_key'] = $video_info['video_high_key'];
			return $video_info;
		}
	}

	/**
	 * [GetListInfo 获取列表型视频信息]
	 * @param string  $type        [获取视频类型]
	 * 0最新视频、1推荐视频、2热播视频、3点赞视频
	 * @param integer $limit       [获取视频条数 默认10条]
	 * @param string  $order [排序方式]
	 */
	public function GetListInfo($type = 0, $row = 10, $order = 'create_time desc'){
		// 根据类型获取数据
		switch ($type) {
			// 最新视频
			case 0:
				// 获取数据
				$data = $this->relation(true)->where(array('status'=> 1))->limit($row)->order($order)->select();
				// 获取真实的封面KEY
				$data = $this->GetThumbKey($data);
				// 组装数据
				foreach ($data as $key => $ret) {
					$data[$key]['url'] = U('Home/Videos/info',array('id'=> $ret['video_id']));
					$data[$key]['video_description'] = empty($ret['video_description']) ? $ret['video_title'] : $ret['video_description'];
				}
				// 返回数据
				return $data;
			break;
			// 推荐视频
			case 1:
				# code...
			break;
			// 热播视频
			case 2:
				# code...
			break;
			// 点赞视频
			case 3:
				# code...
			break;			
			default:
				exit('类型错误');
			break;
		}
	}
	/**
	 * [GetThumbKey 获取真实的封面KEY]
	 * @param [type] $data [description]
	 */
	public function GetThumbKey($data){
		foreach ($data as $key => $ret) {
			if (empty($ret['video_thumb_key'])) {
				// 获取缩略图真实KEY
				$query_thumb = QueryThumb($ret['video_thumb_id'],'player');
				$thumb_key = empty($query_thumb['items'][0]['key']) ? '' : $query_thumb['items'][0]['key'];
				$this->where(array('video_id'=> $ret['video_id']))->save(array('video_thumb_key'=> $thumb_key));
			}else{
				$data[$key]['thumb_info'] = C('QINIU_DOMAIN') . $ret['video_thumb_key'];
			}
		}
		// 返回数据
		return $data;
	}
}