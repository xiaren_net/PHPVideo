-- phpMyAdmin SQL Dump
-- version 3.3.7
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 09 月 08 日 23:26
-- 服务器版本: 5.5.49
-- PHP 版本: 5.6.21

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `7evs_com`
--

-- --------------------------------------------------------

--
-- 表的结构 `php_addons`
--

CREATE TABLE IF NOT EXISTS `php_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='插件安装表' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `php_addons`
--


-- --------------------------------------------------------

--
-- 表的结构 `php_adminc`
--

CREATE TABLE IF NOT EXISTS `php_adminc` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `nick_name` varchar(20) DEFAULT NULL,
  `user_pass` varchar(35) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='超级管理员' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `php_adminc`
--

INSERT INTO `php_adminc` (`uid`, `user_name`, `nick_name`, `user_pass`, `email`, `status`, `create_time`) VALUES
(1, 'admin', '楚羽幽', 'b9abc5b88e396be849de5c681c4b92fa', NULL, 1, 1472251123);

-- --------------------------------------------------------

--
-- 表的结构 `php_auth_group`
--

CREATE TABLE IF NOT EXISTS `php_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='管理组储存表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `php_auth_group`
--

INSERT INTO `php_auth_group` (`id`, `title`, `status`, `rules`, `create_time`) VALUES
(1, '系统管理员', 1, '1,13,14,15,39,2,8,23,24,25,26,9,27,28,29,10,16,17,18,20,19,40,43,44,45,46,3,21,22,30,32,4,11,34,12,35,36,37,38,41,5,62,63,6,33,42,7,47,54,55,64,56,57,58,59,60,61,65,48,49,50,51,52,53', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `php_auth_group_access`
--

CREATE TABLE IF NOT EXISTS `php_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理组与规则对应表';

--
-- 转存表中的数据 `php_auth_group_access`
--

INSERT INTO `php_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `php_auth_rule`
--

CREATE TABLE IF NOT EXISTS `php_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_show` tinyint(4) DEFAULT '1' COMMENT '0隐藏，显示',
  `condition` char(100) NOT NULL DEFAULT '',
  `sort` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='权限规则表' AUTO_INCREMENT=66 ;

--
-- 转存表中的数据 `php_auth_rule`
--

INSERT INTO `php_auth_rule` (`id`, `name`, `title`, `type`, `status`, `is_show`, `condition`, `sort`, `pid`, `create_time`) VALUES
(1, 'Adminc/Index/system', '首页', 1, 1, 1, '', 0, 0, 1472683911),
(2, 'Adminc/Webconfig/system', '系统设置', 1, 1, 1, '', 0, 0, 1472683911),
(3, 'Adminc/Member/system', '用户管理', 1, 1, 1, '', 0, 0, 1472683930),
(4, 'Adminc/Videos/system', '视频管理', 1, 1, 1, '', 0, 0, 1472683945),
(5, 'Adminc/Special/system', '专辑管理', 1, 1, 1, '', 0, 0, 1472683974),
(6, 'Adminc/Theme/system', '其他管理', 1, 1, 1, '', 0, 0, 1472683987),
(7, 'Adminc/Addons/system', '扩展管理', 1, 1, 1, '', 0, 0, 1472684068),
(8, 'Adminc/Group/index', '职位组', 1, 1, 1, '', 0, 2, 1472686303),
(9, 'Adminc/Admins/index', '管理员', 1, 1, 1, '', 0, 2, 1472689360),
(10, 'Adminc/Micrule/index', '权限组', 1, 1, 1, '', 0, 2, 1472689389),
(11, 'Adminc/Videos/index', '视频列表', 1, 1, 1, '', 0, 4, 1472689740),
(12, 'Adminc/Category/index', '视频分类', 1, 1, 1, '', 0, 4, 1472689786),
(13, 'Adminc/Index/admin_info', '我的资料', 1, 1, 0, '', 0, 1, 1472783922),
(14, 'Adminc/Public/admin_out', '立即退出', 1, 1, 0, '', 0, 1, 1472783944),
(15, 'Adminc/Cache/index', '全站缓存', 1, 1, 0, '', 0, 1, 1472783970),
(16, 'Adminc/Micrule/add', '权限添加', 1, 1, 0, '', 0, 10, 1472784231),
(17, 'Adminc/Micrule/edit', '权限编辑', 1, 1, 0, '', 0, 10, 1472784254),
(18, 'Adminc/Micrule/del', '权限删除', 1, 1, 0, '', 0, 10, 1472784274),
(19, 'Adminc/Webconfig/index', '站点设置', 1, 1, 0, '', 0, 2, 1472784462),
(20, 'Adminc/Micrule/GetFile', '权限缓存', 1, 1, 0, '', 0, 10, 1472784599),
(21, 'Adminc/Member/index', '用户列表', 1, 1, 1, '', 0, 3, 1472784815),
(22, 'Adminc/Member/add', '用户添加', 1, 1, 1, '', 0, 3, 1472784841),
(23, 'Adminc/Group/mic_access', '职位授权', 1, 1, 0, '', 0, 8, 1472785228),
(24, 'Adminc/Group/add', '职位添加', 1, 1, 0, '', 0, 8, 1472785370),
(25, 'Adminc/Group/edit', '职位编辑', 1, 1, 0, '', 0, 8, 1472785390),
(26, 'Adminc/Group/del', '职位删除', 1, 1, 0, '', 0, 8, 1472785421),
(27, 'Adminc/Admins/add', '管理员添加', 1, 1, 0, '', 0, 9, 1472785557),
(28, 'Adminc/Admins/edit', '管理员编辑', 1, 1, 0, '', 0, 9, 1472785599),
(29, 'Adminc/Admins/del', '管理员删除', 1, 1, 0, '', 0, 9, 1472785623),
(30, 'Adminc/Member/edit', '用户编辑', 1, 1, 0, '', 0, 3, 1472785780),
(32, 'Adminc/Member/del', '用户删除', 1, 1, 0, '', 0, 3, 1472785944),
(33, 'Adminc/Theme/index', '模版主题', 1, 1, 1, '', 0, 6, 1472786106),
(34, 'Adminc/Videos/edit', '视频编辑', 1, 1, 0, '', 0, 11, 1472786220),
(35, 'Adminc/Category/add', '分类添加', 1, 1, 0, '', 0, 12, 1472786244),
(36, 'Adminc/Category/edit', '分类编辑', 1, 1, 0, '', 0, 12, 1472786262),
(37, 'Adminc/Category/del', '分类删除', 1, 1, 0, '', 0, 12, 1472786282),
(38, 'Adminc/Category/GetFile', '缓存分类', 1, 1, 0, '', 0, 12, 1472786303),
(39, 'Adminc/Cache/options', '开始缓存', 1, 1, 0, '', 0, 1, 1472787806),
(40, 'Adminc/Menus/index', '菜单管理', 1, 1, 1, '', 0, 6, 1473061016),
(41, 'Adminc/Videos/shenhe', '审核视频', 1, 1, 1, '', 0, 4, 1473064224),
(42, 'Adminc/Links/index', '友情连接', 1, 1, 1, '', 0, 6, 1473067821),
(43, 'Adminc/Menus/add', '添加菜单', 1, 1, 0, '', 0, 40, 1473089797),
(44, 'Adminc/Menus/GetFile', '更新缓存', 1, 1, 0, '', 0, 40, 1473089819),
(45, 'Adminc/Menus/edit', '编辑菜单', 1, 1, 0, '', 0, 40, 1473089844),
(46, 'Adminc/Menus/del', '删除菜单', 1, 1, 0, '', 0, 40, 1473089860),
(47, 'Adminc/Addons/index', '插件管理', 1, 1, 1, '', 0, 7, 1473186164),
(48, 'Adminc/Addons/hooks', '钩子管理', 1, 1, 1, '', 0, 7, 1473186231),
(49, 'Adminc/Addons/addhook', '创建钩子视图', 1, 1, 0, '', 0, 48, 1473187398),
(50, 'Adminc/Addons/edithook', '编辑钩子视图', 1, 1, 0, '', 0, 48, 1473187476),
(51, 'Adminc/Addons/delhook', '超级管理删除钩子', 1, 1, 0, '', 0, 48, 1473187821),
(52, 'Adminc/Addons/updateHook', '创建或更新钩子', 1, 1, 0, '', 0, 48, 1473187871),
(53, 'Adminc/Addons/execute', '执行钩子', 1, 1, 0, '', 0, 48, 1473187916),
(54, 'Adminc/Addons/create', '创建插件', 1, 1, 0, '', 0, 47, 1473194508),
(55, 'Adminc/Addons/config', '配置插件', 1, 1, 0, '', 0, 47, 1473194567),
(56, 'Adminc/Addons/saveConfig', '保存插件设置', 1, 1, 0, '', 0, 47, 1473194617),
(57, 'Adminc/Addons/install', '安装插件', 1, 1, 0, '', 0, 47, 1473194651),
(58, 'Adminc/Addons/uninstall', '卸载插件', 1, 1, 0, '', 0, 47, 1473194683),
(59, 'Adminc/Addons/enable', '启用插件', 1, 1, 0, '', 0, 47, 1473196876),
(60, 'Adminc/Addons/disable', '禁用插件', 1, 1, 0, '', 0, 47, 1473196909),
(61, 'Adminc/Addons/build', '执行创建', 1, 1, 0, '', 0, 47, 1473213730),
(62, 'Adminc/Special/index', '专辑列表', 1, 1, 1, '', 0, 5, 1473241812),
(63, 'Adminc/Special/category', '专辑分类', 1, 1, 1, '', 0, 5, 1473241855),
(64, 'Adminc/Files/UploadOnePicture', '上传图片', 1, 1, 0, '', 0, 55, 1473249111),
(65, 'Adminc/Addons/adminList', '数据管理', 1, 1, 0, '', 0, 47, 1473257189);

-- --------------------------------------------------------

--
-- 表的结构 `php_category`
--

CREATE TABLE IF NOT EXISTS `php_category` (
  `cate_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='视频分类数据表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `php_category`
--

INSERT INTO `php_category` (`cate_id`, `title`, `name`, `description`, `keywords`, `sort`, `pid`, `create_time`) VALUES
(1, 'ddasdas', '网络微电影', 'dsadas', 'dasdas', 0, 0, 1472325243);

-- --------------------------------------------------------

--
-- 表的结构 `php_hooks`
--

CREATE TABLE IF NOT EXISTS `php_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='钩子数据表' AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `php_hooks`
--

INSERT INTO `php_hooks` (`id`, `name`, `description`, `type`, `update_time`, `addons`) VALUES
(1, 'pageHeader', '页面header钩子，一般用于加载插件CSS文件和代码', 1, 1472325243, ''),
(2, 'pageFooter', '页面footer钩子，一般用于加载插件JS文件和JS代码', 1, 1472325243, ''),
(3, 'documentEditForm', '添加编辑表单的 扩展内容钩子', 1, 1472325243, ''),
(4, 'documentDetailAfter', '页面末尾显示', 1, 1473331303, ''),
(5, 'documentDetailBefore', '页面内容前显示用钩子', 1, 1473331345, ''),
(6, 'documentSaveComplete', '保存文档数据后的扩展钩子', 2, 1472325243, ''),
(7, 'documentEditFormContent', '添加编辑表单的内容显示钩子', 1, 1472325243, ''),
(8, 'adminArticleEdit', '后台内容编辑页编辑器', 1, 1472325243, ''),
(13, 'AdminIndex', '首页小格子个性化显示', 1, 1472325243, ''),
(14, 'topicComment', '评论提交方式扩展钩子。', 1, 1472325243, ''),
(16, 'app_begin', '应用开始', 1, 1472325243, ''),
(18, 'VideoDetail', '视频播放内容页钩子', 1, 1473331393, 'Ckplayer');

-- --------------------------------------------------------

--
-- 表的结构 `php_links`
--

CREATE TABLE IF NOT EXISTS `php_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='友情连接数据表' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `php_links`
--


-- --------------------------------------------------------

--
-- 表的结构 `php_member`
--

CREATE TABLE IF NOT EXISTS `php_member` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(15) DEFAULT NULL,
  `nick_name` varchar(20) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户数据表' AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `php_member`
--

INSERT INTO `php_member` (`user_id`, `mobile`, `nick_name`, `user_pass`, `avatar`, `create_time`) VALUES
(1, '18788654245', '楚羽幽', 'MDAwMDAwMDAwMLSJjmyApnxpr3S7lw', NULL, 1472272204),
(2, '18788654248', '楚羽幽2d', 'MDAwMDAwMDAwML6Mi6yTuWt0', NULL, 1472272235),
(3, '18785305198', '任龙', 'MDAwMDAwMDAwMLSJjmyApnxpr3S7lw', NULL, 1472272235),
(4, '18788654246', '天风', 'MDAwMDAwMDAwMLSJjmyApnxpr3S7lw', NULL, 1472949520),
(5, '13444444444', '根据', 'MDAwMDAwMDAwMMqjoaKWz4Gsw4Gjnw', NULL, 1472949926),
(6, '13466666666', '66666666', 'MDAwMDAwMDAwMLOfkmqAzJBpr5q_nw', NULL, 1472950892),
(7, '13892085050', '123456', 'MDAwMDAwMDAwMLKJgrKApoxp', NULL, 1472967450);

-- --------------------------------------------------------

--
-- 表的结构 `php_menus`
--

CREATE TABLE IF NOT EXISTS `php_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='前台菜单数据表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `php_menus`
--

INSERT INTO `php_menus` (`id`, `title`, `url`, `status`, `sort`, `pid`, `create_time`) VALUES
(1, '首页', 'http://www.7evs.com', 1, 1, 0, 1473091322),
(2, '视频排行榜', '#name', 1, 3, 0, 1473092413),
(3, '最新视频', 'Home/Category/index', 1, 2, 0, 1473094519);

-- --------------------------------------------------------

--
-- 表的结构 `php_picture`
--

CREATE TABLE IF NOT EXISTS `php_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片储存表' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `php_picture`
--


-- --------------------------------------------------------

--
-- 表的结构 `php_videos`
--

CREATE TABLE IF NOT EXISTS `php_videos` (
  `video_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video_cid` int(11) DEFAULT '0',
  `video_uid` int(11) DEFAULT '0',
  `video_key` varchar(50) DEFAULT NULL,
  `video_sd_id` varchar(50) DEFAULT NULL,
  `video_high_id` varchar(100) DEFAULT NULL,
  `video_super_id` varchar(100) DEFAULT NULL,
  `video_sd_key` varchar(100) DEFAULT NULL COMMENT '转码后的标清KEY',
  `video_high_key` varchar(100) DEFAULT NULL COMMENT '转码后的高清KEY',
  `video_super_key` varchar(100) DEFAULT NULL COMMENT '转码后的超清KEY',
  `video_title` varchar(100) DEFAULT NULL,
  `video_keywords` varchar(50) DEFAULT NULL,
  `video_description` text,
  `video_thumb_id` varchar(100) DEFAULT NULL COMMENT '视频截图ID',
  `video_thumb_key` varchar(100) DEFAULT NULL COMMENT '截图后的KEY',
  `status` tinyint(1) DEFAULT '0' COMMENT '0 未审核，1已审核',
  `play_times` int(11) DEFAULT '0' COMMENT '播放次数',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='视频数据表' AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `php_videos`
--

INSERT INTO `php_videos` (`video_id`, `video_cid`, `video_uid`, `video_key`, `video_sd_id`, `video_high_id`, `video_super_id`, `video_sd_key`, `video_high_key`, `video_super_key`, `video_title`, `video_keywords`, `video_description`, `video_thumb_id`, `video_thumb_key`, `status`, `play_times`, `create_time`) VALUES
(1, 1, 1, 'lvQPszg90gcbVTdbLFXDO1kLjqAa', 'z0.57cb81ae7823de7b57012b67', 'z0.57cb81ae7823de7b57012b68', 'z0.57cb81ae7823de7b57012b6a', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lvQPszg90gcbVTdbLFXDO1kLjqAa', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lvQPszg90gcbVTdbLFXDO1kLjqAa', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lvQPszg90gcbVTdbLFXDO1kLjqAa', '寻找果实之谜', NULL, NULL, 'z0.57cb81ea7823de7b57012f58', 'TcUqxpQuixOvfLWscVWj51MJFiA=/lo6lPpDEq2KA7hdxbHlMOIee9yL6', 1, 22, 1472954798),
(2, 1, 1, 'lsWYnc7UquAj0zVPZFo6Axa3SgXB', 'z0.57cb89087823de7b5701b4ac', 'z0.57cb89087823de7b5701b4af', 'z0.57cb89087823de7b5701b4b1', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lsWYnc7UquAj0zVPZFo6Axa3SgXB', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lsWYnc7UquAj0zVPZFo6Axa3SgXB', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lsWYnc7UquAj0zVPZFo6Axa3SgXB', '《兄弟抱一下》', NULL, NULL, 'z0.57cb89087823de7b5701b4b2', 'NZMHXo4XCTpnOwc7PelohWN6W1s=/lsWYnc7UquAj0zVPZFo6Axa3SgXB', 1, 13, 1472956680),
(3, 1, 7, 'lvW8Yo-x6pQsrC8NoTn22miDKrQQ', 'z0.57cbb4e47823de7b57053054', 'z0.57cbb4e47823de7b57053057', 'z0.57cbb4e47823de7b5705305b', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lvW8Yo-x6pQsrC8NoTn22miDKrQQ', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lvW8Yo-x6pQsrC8NoTn22miDKrQQ', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lvW8Yo-x6pQsrC8NoTn22miDKrQQ', 'BZ2015ZYNNYSHF', '', '', 'z0.57cbb4f17823de7b570531d7', 'PePswmKREv0jqKycQlkSixYZGyA=/lvW8Yo-x6pQsrC8NoTn22miDKrQQ', 1, 4, 1472967919),
(4, 1, 1, 'lpt_oN5y3ItGi5HuD2jSunmPMGec', 'z0.57cbf8d17823de7b570abbd1', 'z0.57cbf8d17823de7b570abbd2', 'z0.57cbf8d17823de7b570abbd3', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lpt_oN5y3ItGi5HuD2jSunmPMGec', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lpt_oN5y3ItGi5HuD2jSunmPMGec', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lpt_oN5y3ItGi5HuD2jSunmPMGec', '一生所爱', '一生所爱-卢冠廷', '一生所爱-卢冠廷', 'z0.57cbf8d27823de7b570abbd6', '1ftzPB-tK17M2L5dt69oBsWf3S0=/lpt_oN5y3ItGi5HuD2jSunmPMGec', 1, 3, 1472985313),
(5, 1, 1, 'lnuWNb-Dika-OR_61YCy04YjTvpT', 'z0.57cd33817823de7b571eb8a5', 'z0.57cd33817823de7b571eb8a6', 'z0.57cd33817823de7b571eb8a7', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lnuWNb-Dika-OR_61YCy04YjTvpT', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lnuWNb-Dika-OR_61YCy04YjTvpT', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lnuWNb-Dika-OR_61YCy04YjTvpT', '视频播放器', NULL, NULL, 'z0.57cd34cb7823de7b571ed16c', 'on4xJWOhkPWbkzvPU8rfc77WfHg=/lgxQoq868kwCdkgVxz4rUIpFLSFz', 1, 4, 1473065857),
(6, 1, 1, 'lmfG4lJXqiNPum4mSFA0Hax1UZiB', 'z0.57cd35ca7823de7b571ee314', 'z0.57cd35ca7823de7b571ee319', 'z0.57cd35ca7823de7b571ee31c', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lmfG4lJXqiNPum4mSFA0Hax1UZiB', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lmfG4lJXqiNPum4mSFA0Hax1UZiB', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lmfG4lJXqiNPum4mSFA0Hax1UZiB', '贵州合锐科技宣传视频', NULL, NULL, 'z0.57cd364b7823de7b571eeb47', 'n2AIoJTMjaLIBnfBmJeLhaecQDI=/lnTYIuJuN8_i7S8L53OUKRcqiDjQ', 1, 9, 1473066443),
(7, 1, 1, 'lkGPqOiUOMl1BpgeBR3GkxC9ufKG', 'z0.57cd36ae7823de7b571ef153', 'z0.57cd36ae7823de7b571ef155', 'z0.57cd36ae7823de7b571ef157', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lkGPqOiUOMl1BpgeBR3GkxC9ufKG', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lkGPqOiUOMl1BpgeBR3GkxC9ufKG', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lkGPqOiUOMl1BpgeBR3GkxC9ufKG', '《我要看电视》第二期', NULL, NULL, 'z0.57cd36ae7823de7b571ef15b', 'I4M9CWTm7c5f0WFLVwvcOvygHmY=/lkGPqOiUOMl1BpgeBR3GkxC9ufKG', 1, 10, 1473066670),
(8, 1, 1, 'llZkAr3MSL9pLq9-4WIRlJsyTD1H', 'z0.57cd37b07823de7b571f0271', 'z0.57cd37b07823de7b571f0273', 'z0.57cd37b07823de7b571f0275', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/llZkAr3MSL9pLq9-4WIRlJsyTD1H', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/llZkAr3MSL9pLq9-4WIRlJsyTD1H', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/llZkAr3MSL9pLq9-4WIRlJsyTD1H', '2009年9月-『武林MV-神话』', NULL, NULL, 'z0.57cd37b07823de7b571f0276', 'OHefkI4a6ar9CNxbpnTkWaD6nI0=/llZkAr3MSL9pLq9-4WIRlJsyTD1H', 1, 8, 1473066928),
(9, 1, 1, 'ljSOPQBoetGCHgVJEdIq4WLlKgim', 'z0.57cd37da7823de7b571f0608', 'z0.57cd37da7823de7b571f0609', 'z0.57cd37da7823de7b571f060c', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/ljSOPQBoetGCHgVJEdIq4WLlKgim', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/ljSOPQBoetGCHgVJEdIq4WLlKgim', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/ljSOPQBoetGCHgVJEdIq4WLlKgim', '2010年6月-『武林MV-千年泪』', NULL, NULL, 'z0.57cd37da7823de7b571f060d', 'jxrQfaW7ReHDvtv3qvHXOuyvoMA=/ljSOPQBoetGCHgVJEdIq4WLlKgim', 1, 123, 1473066970),
(10, 1, 1, 'lty_o-mAFgYE7_mqtnv7Ig34Sou5', 'z0.57cd37fc7823de7b571f0832', 'z0.57cd37fd7823de7b571f0834', 'z0.57cd37fd7823de7b571f0837', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lty_o-mAFgYE7_mqtnv7Ig34Sou5', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lty_o-mAFgYE7_mqtnv7Ig34Sou5', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lty_o-mAFgYE7_mqtnv7Ig34Sou5', '2010年10月-『武林MV-朱砂泪』', NULL, NULL, 'z0.57cd37fd7823de7b571f083a', 'z9pHaYxSAZDM32guMj4Ot0ii6iw=/lty_o-mAFgYE7_mqtnv7Ig34Sou5', 1, 19, 1473067005),
(11, 1, 1, 'lq3NJ5rlLQp3Y9VFOJzlKO4-x9Lj', 'z0.57cd38207823de7b571f0aa1', 'z0.57cd38207823de7b571f0aa2', 'z0.57cd38207823de7b571f0aa6', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lq3NJ5rlLQp3Y9VFOJzlKO4-x9Lj', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lq3NJ5rlLQp3Y9VFOJzlKO4-x9Lj', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lq3NJ5rlLQp3Y9VFOJzlKO4-x9Lj', '2011年1月-『武林MV-半月琴』', NULL, NULL, 'z0.57cd38207823de7b571f0aab', 'Vp-MUjuAH3xV73PfhUDeZomdQ_U=/lq3NJ5rlLQp3Y9VFOJzlKO4-x9Lj', 1, 19, 1473067040),
(12, 1, 1, 'lsTkKcgT__6s8Bryd6gDMR_qEUAM', 'z0.57cd384e7823de7b571f0ecd', 'z0.57cd384e7823de7b571f0ecf', 'z0.57cd384e7823de7b571f0ed1', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lsTkKcgT__6s8Bryd6gDMR_qEUAM', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lsTkKcgT__6s8Bryd6gDMR_qEUAM', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lsTkKcgT__6s8Bryd6gDMR_qEUAM', '冰火劫-灭', NULL, NULL, 'z0.57cd384e7823de7b571f0ed5', 'lP5KU5BHG9JdtAkksbE7ug39EQs=/lsTkKcgT__6s8Bryd6gDMR_qEUAM', 1, 9, 1473067086),
(13, 1, 1, 'lstYy_KqHArRbA82nDxMo91eGOHF', 'z0.57cdcda07823de7b5729f7c2', 'z0.57cdcda07823de7b5729f7c3', 'z0.57cdcda17823de7b5729f7c4', '3pCjiuQH7VbyydGzLWYJOmUpMJU=/lstYy_KqHArRbA82nDxMo91eGOHF', 'OkTggBFHw2CbnlhxVUfkROU0RAw=/lstYy_KqHArRbA82nDxMo91eGOHF', '-Vh83UbmrvHfcLf0yVHn8A0oZRo=/lstYy_KqHArRbA82nDxMo91eGOHF', '胡敏-曾经的你', NULL, NULL, 'z0.57cdcda17823de7b5729f7c5', '2Ux_QE-5mEhTV8xUAcUqbFT7gIU=/lstYy_KqHArRbA82nDxMo91eGOHF', 1, 49, 1473105313);

-- --------------------------------------------------------

--
-- 表的结构 `php_video_single`
--

CREATE TABLE IF NOT EXISTS `php_video_single` (
  `sid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(11) DEFAULT NULL,
  `video_uid` int(11) DEFAULT NULL,
  `video_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频播单数据表' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `php_video_single`
--

