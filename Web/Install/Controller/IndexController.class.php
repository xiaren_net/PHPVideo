<?php
namespace Install\Controller;
use Think\Controller;
use Common\Util\Dir;
class IndexController extends Controller {

	/**
	 * [_initialize 构造函数]
	 * @return [type] [description]
	 */
	public function _initialize(){
        // 访问操作权限
        $_action = array(
            'isLock',
            'Complete'            
        );
		if (file_exists('Data/Install.lock') && !in_array(ACTION_NAME, $_action)) {
            $this->redirect('Install/Index/isLock');
        }
	}

	/**
	 * [index 安装默认页面]
	 * @return [type] [description]
	 */
    public function index(){
    	$install = file_get_contents(MODULE_PATH . 'install.txt');
    	$this->assign('install', $install);
        $this->display();
    }
    /**
     * [islock 安装锁定]
     * @return [type] [description]
     */
    public function islock(){
    	$this->display();
    }

    /**
     * [validates 环境验证]
     * @return [type] [description]
     */
    public function validates(){
    	// 环境检测
    	$this->safe = (ini_get('safe_mode') ? '<span class="text-red">未开启</span>' : '<span class="text-main">正常</span>');
        $this->gd = extension_loaded('GD') ? '<span class="text-main">正常</span>' : '<span class="text-red">未开启</span>';
        $this->mod_rewrite = in_array('mod_rewrite', apache_get_modules()) ? '<span class="text-main">正常</span>' : '<span class="text-red">未开启</span>';
        $this->curl = extension_loaded('CURL') ? '<span class="text-main">正常</span>' : '<span class="text-red">未开启</span>';
        $this->mysqli = function_exists("mysqli_connect") ? '<span class="text-main">正常</span>' : '<span class="text-red">未开启</span>';
        $this->mb_substr = function_exists("mb_substr") ? '<span class="text-main">正常</span>' : '<span class="text-red">未开启</span>';
    	// 目录权限检测
    	$this->dir = array(
    		"Data/Config/Web.config.php", //站点配置文件
            "Data/Config/Db.config.php", //数据库配置文件
    		'Cache',
    		'Data/Backup',
    		'Data/Config',
    		'Upload',
    	);
    	$this->display();
    }

    /**
     * [database 设置数据库]
     * @return [type] [description]
     */
    public function database(){
        if(IS_POST){
            if(!$lnk = mysql_connect($_POST['DB_HOST'],$_POST['DB_USER'],$_POST['DB_PWD'])){
                $this->error('数据库账户或密码错误！');
                return false;
            }
            // 判断数据库是否存在
            if (!mysql_select_db($_POST['DB_NAME'], $lnk)) {
            	$this->error('数据库不存在，请先手动创建空数据库');
            	return false;
            }
            // 获取SQL文件
            $files = file_get_contents(MODULE_PATH . '/Data/7evs_com.sql');
            $_arr = explode(';', $files);
            foreach ($_arr as $key => $ret) {
                mysql_query(str_replace('php_', $_POST['DB_PREFIX'], $ret) . ';');
            }
            // 修改配置文件
            $config = array(
            	'DB_TYPE'               =>  'mysqli',
            	'DB_HOST'               =>  $_POST['DB_HOST'],
            	'DB_NAME'               =>  $_POST['DB_NAME'],
            	'DB_USER'               =>  $_POST['DB_USER'],
            	'DB_PWD'                =>  $_POST['DB_PWD'],
            	'DB_PORT'               =>  $_POST['DB_PORT'],
            	'DB_PREFIX'             =>  $_POST['DB_PREFIX'],
            	'DB_CHARSET'            =>  'utf8',
            );
            $data = "<?php \nreturn ".var_export($config,true).";\n";
            // 写入数据库配置
            file_put_contents('Data/Config/Db.config.php',$data);
            // 更新管理员
            mysql_query("UPDATE `adminc` set `user_name`='$_POST[admin_user]','user_pass'='$user_pass',`email`='$_POST[admin_email]' WHERE `user_id`=`;");

            // 修改站点信息配置文件
            $webconfig =  array(
                'MIC_WEB_NAME'		=> C('MIC_WEB_NAME'),
                'MIC_DOMAIN'		=> C('MIC_DOMAIN'),
                'MIC_TITLE'			=> C('MIC_TITLE'),
                'MIC_KEYWORDS'		=> C('MIC_KEYWORDS'),
                'MIC_DESCRIPTION'	=> C('MIC_DESCRIPTION'),
            );
            // 转字符集
            $data = "<?php \nreturn ".var_export($webconfig,true).";\n?>";
            // 写入站点配置文件
            file_put_contents('Data/Config/Web.config.php',$data);
            // 写入锁定文件
            file_put_contents('Data/Install.lock', C('SYSTEM_WEBNAME') . "\n安装完成！本文件为程序锁定文件，请勿删除。");
            $this->success(C('SYSTEM_WEBNAME') . '，安装完成！', U('Index/Complete'));
        }else{
            $this->display();
        }
    }

    /**
     * [Complete 安装完成]
     */
    public function Complete(){
        $this->display();
    }
}