<?php
/**
 * 自定义扩展标签库
 * Mic工作室 www.16mic.com
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Common\TagLib;
use Think\Template\TagLib;
class Cy extends TagLib{

    // 标签定义
    protected $tags   =  array(
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次

        // 拼图框架
        'Pintuer' =>  array('attr'=>'','close'=>0),
        // Layer弹出层
        'Layer' =>  array('attr'=>'','close'=>0),
        // 获取菜单数据标签
        'Menus' =>  array('attr'=>'row,order', 'close'=>1, 'level'=> 3),
        // 视频数据标签
        'Videos' =>  array('attr'=>'type,row,order', 'close'=>1, 'level'=> 3),
    );

    /**
     * [_Category 商品分类数据标签]
     * @param  [type] $attr    [标签属性]
     * @param  [type] $content [标签内容]
     * @return [type]          [description]
     */
    public function _Menus($attr, $content){
        // 获取数量
        $row = isset($attr['row']) ? $attr['row'] : 10;
        // 排序字段,排序方式小写逗号隔开
        $order = isset($attr['order']) ? $attr['order'] : 'create_time,asc';
        
        // 数据处理
        $php = <<<str
        <?php
        \$menus_module = D('Home/Menus');
        \$Menus_list = \$menus_module->GetMenusList('$row',"$order");

        // 循环遍历
        foreach (\$Menus_list as \$menu_key => \$menu_info):
        ?>
str;
        $php .= $content;
        $php .= "<?php endforeach;?>";
        return $php;
    }

    /**
     * [_Videos 视频数据标签]
     * @param  [type] $attr    [标签属性]
     * @param  [type] $content [标签内容]
     * @return [type]          [description]
     */
    public function _Videos($attr, $content){
        // 获取类型 0最新视频、1推荐视频、2热播视频、3点赞视频
        $type = isset($attr['type']) ? $attr['type'] : 0;
        // 获取数量
        $row = isset($attr['row']) ? $attr['row'] : 10;
        // 排序方式 asc正序 desc倒序 nat 自然排序
        $order = isset($attr['order']) ? $attr['order'] : 'create_time desc';
        
        // 数据处理
        $php = <<<str
        <?php
        \$model = D('Home/Videos');
        \$videos_info = \$model->GetListInfo("$type", $row,"$order");

        // 循环遍历
        foreach (\$videos_info as \$video_key => \$video_info):
        ?>
str;
        $php .= $content;
        $php .= "<?php endforeach;?>";
        return $php;
    }
    /**
     * 拼图Pintuer 标签
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return active
     */
    public function _Pintuer($tag,$content) {
        return "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n<script type=\"text/javascript\" src=\"__PUBLIC__/Pintuer/jquery-1.11.0.js\"></script>\n<link rel=\"stylesheet\" type=\"text/css\" href=\"__PUBLIC__/Pintuer/pintuer.css\" />\n<script type=\"text/javascript\" src=\"__PUBLIC__/Pintuer/pintuer.js\"></script>\n<script type=\"text/javascript\" src=\"__PUBLIC__/Pintuer/respond.js\"></script>";
    }
    
    /**
     * [_Layer Layer弹出层]
     * @param  [type] $tag     [description]
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    public function _Layer($tag,$content) {
        return "<script type=\"text/javascript\" src=\"__PUBLIC__/Layer/layer.js\"></script>";
    }
}