<?php
$result = array(
    /*--------------------------调试相关-----------------------------------*/
    // 'SHOW_PAGE_TRACE'       => true, 

	/*--------------------------自定义标签库-----------------------------------*/
	'APP_AUTOLOAD_PATH'		=> '@.Taglib',
	'TAGLIB_BUILD_IN'		=> 'Cx,Common\TagLib\Cy',
	'TAGLIB_PRE_LOAD'		=> 'Cx,Common\TagLib\Cy',

	/*--------------------------第三方类库-----------------------------------*/
	'AUTOLOAD_NAMESPACE'    => array(
		'Common'            => APP_PATH . 'Common',
        'Addons'            => MIC_ADDONS_PATH
	),

    /*--------------------------路由相关-----------------------------------*/
    'URL_MODEL'             => 2,
    'URL_CASE_INSENSITIVE'  => false, // 默认false 表示URL区分大小写 true则表示不区分大小写

	/*--------------------------程序信息-----------------------------------*/
    'SYSTEM_COPYRIGHT'      =>  '奇亦微视网',
    'SYSTEM_WEBNAME'		=>  '奇亦视频管理系统',
    'SYSTEM_AUTHOR'			=>  '楚羽幽',
    'SYSTEM_VERSION'		=>  'Beta V0.3',
    'SYSTEM_VERSION_TIME'	=>  '1473202883',
    'SYSTEM_EMAIL'          =>  'Name_Cyu@Foxmail.Com',
    'SYSTEM_QQ'             =>  '1113581489',
    'SYSTEM_FLOCK'			=>  '',
    'SYSTEM_DOMAIN'			=>  'http://www.7evs.com',
);
return array_merge($result,require 'Data/Config/Db.config.php',require 'Data/Config/Qiniu.config.php',require 'Data/Config/Web.config.php',require 'Data/Config/Status.config.php');