<?php
return array(
	/****************************后台错误提示模板**************************************/
	'VIEW_PATH'				=>	'',	//视图模版路径
	'DEFAULT_THEME'			=>	'',	//默认模板主题
	'THEME_LIST'        	=>  '',//模板主题列表
    'TMPL_TEMPLATE_SUFFIX'  =>  '.php',
    /*--------------------------模板相关配置-----------------------------------*/
    'TMPL_PARSE_STRING' => array(
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
        '__IMG__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
        '__CSS__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
        '__JS__'     => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
    ),
    /*--------------------------图片上传相关配置-----------------------------------*/
    'PICTURE_UPLOAD' => array(
        'exts'      => array(),
        'maxSize'   => 5*1024*1024, //上传的文件大小限制 (0-不做限制)
        'exts'      => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub'   => true, //自动子目录保存文件
        'subName'   => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath'  => './Uploads/',
        'savePath'  => 'Picture/',
        'replace'   => false,
        'saveExt'   =>  'jpg',
        'saveName'  => array('uniqid', ''),
        'hash'     => true, //是否生成hash编码        
    ), //图片上传相关配置（文件上传类配置）

    /****************************后台错误提示模板**************************************/
    'TMPL_ACTION_ERROR'     =>  'Public/error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  'Public/success.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  'Public/exception.html',// 异常页面的模板文件
);