<?php
/**
 * MIC网络信息技术
 * 管理员控制器
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Controller;
class AdminsController extends BaseController{
	public function _initialize(){
		parent::_initialize();
		$this->db = D('Adminc');
	}

	// 管理员列表
	public function index(){
		$data = $this->db->relation(true)->order('uid asc')->select();

		$this->assign('data', $data);
		$this->display();
	}

	/**
	 * [add 新增管理]
	 */
	public function add(){
		if (IS_POST) {
			if ($this->db->admin_add()) {
				$this->success('操作成功', U('index'));
			}else{
				$this->error($this->db->getError());
			}
		}else{
			$data = M('AuthGroup')->where(array('status'=> 1))->select();

			$this->assign('data', $data);
			$this->display();
		}
	}

	/**
	 * [edit 修改管理员]
	 * @return [type] [description]
	 */
	public function edit(){
		if (IS_POST) {
			if ($this->db->admin_edit()) {
				$this->success('操作成功', U('index'));
			}else{
				$this->error($this->db->getError());
			}
		}else{
			$uid = I('uid', 0, 'intval');
			$field = $this->db->where(array('uid'=> $uid))->find();
			$data = M('AuthGroup')->where(array('status'=> 1))->select();
			$group_ac = M('AuthGroupAccess')->where(array('uid'=> $uid))->find();

			$this->assign('data', $data);
			$this->assign('group_ac', $group_ac);
			$this->assign('field', $field);
			$this->display();
		}
	}

	/**
	 * [del 删除管理员]
	 * @return [type] [description]
	 */
	public function del(){
		if ($this->db->admin_del()) {
			$this->success('操作成功');
		}else{
			$this->error($this->db->getError());
		}
	}
}