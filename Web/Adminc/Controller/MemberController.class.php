<?php
namespace Adminc\Controller;
class MemberController extends BaseController {

	// 构造函数
	public function _initialize(){
		parent::_initialize();
		$this->db = D('Member');
	}
	/**
     * [index 用户列表]
     * @return [type] [description]
     */
    public function index(){
    	$data = $this->db->order('create_time desc')->field('user_pass',true)->select();
    	$this->assign('data', $data);
        $this->display();
    }

    /**
     * [add 添加新用户]
     */
    public function add(){
        if (IS_POST) {
            if ($this->db->member_add()) {
                $this->success('操作成功');
            }else{
                $this->error($this->db->getError());
            }
        }else{
            $this->display();
        }
    }

    /**
     * [edit description]
     * @return [type] [description]
     */
    public function edit(){
        if (IS_POST) {
            if ($this->db->member_edit()) {
                $this->success('操作成功');
            }else{
                $this->error($this->db->getError());
            }
        }else{
            $user_id = intval($_GET['uid']);
            $user_info = $this->db->where(array('user_id'=> $user_id))->find();
            $this->assign('user_info', $user_info);
            $this->display();
        }
    }

    /**
     * [del description]
     * @return [type] [description]
     */
    public function del(){

    }
}