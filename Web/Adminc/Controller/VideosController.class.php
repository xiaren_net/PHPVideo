<?php
namespace Adminc\Controller;
use Think\Page;
class VideosController extends BaseController {

	// 构造函数
	public function _initialize(){
		parent::_initialize();
		$this->db = D('Videos');
	}

	// 视频列表
    public function index(){
        // 统计数据总数
        $count = $this->db->where(array('status'=> 1))->count();
        // 实例化分页类 传入总记录数和每页显示的记录数
        $page = new Page($count,15);
        // 样式制定
        $page->lastSuffix = false;
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->setConfig('header', '共 <span class="text-red">%TOTAL_ROW%</span> 个视频');
        $page->setConfig('theme', '%UP_PAGE% %FIRST% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER% 总 <span class="text-red">%TOTAL_PAGE%</span> 页');
        // 显示数据分页
        $show = $page->show();
    	$data = $this->db->relation(true)->where(array('status'=> 1))->order('create_time desc')->limit($page->firstRow.','.$page->listRows)->select();
        // 视频转码状态
        $data = $this->QueryVideo($data);
    	$video_status = C('VIDEO_STATUS');
    	$this->assign('video_status', $video_status);
        $this->assign('page', $show);
    	$this->assign('data', $data);
        $this->display();
    }

    /**
     * [edit 视频编辑]
     * @return [type] [description]
     */
    public function edit(){
    	if (IS_POST) {
    		if ($this->db->VideoSave()) {
				$this->success('视频编辑操作成功');
			}else{
				$this->error($this->db->getError());
			}
    	}else{
    		// 获取数据
    		$video_id = intval($_GET['id']);
    		$video_info = $this->db->relation(true)->where(array('video_id'=> $video_id))->find();
            // 查询视频转码KEY
            $video_info['zhuanma_key'] = $this->VideoKeyStatus($video_info['video_high_id']);
    		// 获取缩略图
            $video_info = $this->ThumbInfo(object_array(json_decode(file_get_contents("http://api.qiniu.com/status/get/prefop?id=".$video_info['video_thumb_id']))), $video_info);
    		// 分配数据
    		$this->assign('category', S('Category'));
    		$this->assign('video_info', $video_info);
    		$this->display();
    	}
    }

    /**
     * [shenhe 审核视频]
     * @return [type] [description]
     */
    public function shenhe(){
        if (IS_AJAX) {
            // 获取数据
            $type = isset($_GET['type']) ? $_GET['type'] : 'no';
            $id = intval($_GET['id']);
            // 转码状态
            $video_info = $this->db->relation(true)->where(array('video_id'=> $id))->find();
            $this->db->GetVideoKey($video_info);        // 视频转码
            $this->db->GetThumbKey($video_info);        // 视频截图
            // 操作状态
            switch ($type) {
                case 'yes':
                    if ($this->db->where(array('video_id'=> $id))->save(array('status'=> 1))) {
                        $this->success('视频状态操作成功');
                    }else{
                        $this->error('视频状态操作失败', U('shenhe'));
                    }
                break;
                default:
                    if ($this->db->where(array('video_id'=> $id))->save(array('status'=> 0))) {
                        $this->success('视频状态操作成功');
                    }else{
                        $this->error('视频状态操作失败', U('shenhe'));
                    }
                break;
            }
        }else{
            // 统计数据总数
            $count = $this->db->where(array('status'=> 0))->count();
            // 实例化分页类 传入总记录数和每页显示的记录数
            $page = new Page($count,15);
            // 样式制定
            $page->lastSuffix = false;
            $page->setConfig('prev', '上一页');
            $page->setConfig('next', '下一页');
            $page->setConfig('header', '共 <span class="text-red">%TOTAL_ROW%</span> 个视频');
            $page->setConfig('theme', '%UP_PAGE% %FIRST% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER% 总 <span class="text-red">%TOTAL_PAGE%</span> 页');
            // 显示数据分页
            $show = $page->show();
            // 审核视频列表
            $data = $this->db->relation(true)->where(array('status'=> 0))->order('create_time desc')->limit($page->firstRow.','.$page->listRows)->select();
            // 视频转码状态
            $data = $this->QueryVideo($data);
            $video_status = C('VIDEO_STATUS');
            $this->assign('video_status', $video_status);
            $this->assign('page', $show);
            $this->assign('data', $data);
            $this->display();
        }
    }

    /**
     * [VideoTranscodingStatus 获取转码后的KEY]
     * @param string $video_transcoding_id [description]
     * @param [type] $hash                 [description]
     */
    public function VideoKeyStatus($video_transcoding_id = ''){
        $zhuanma_info = VideoKeyStatus($video_transcoding_id, 'player');
        if (empty($zhuanma_info['items'][0]['key'])) {
            return $hash;
        }else{
            return $zhuanma_info['items'][0]['key'];
        }
    }

    /**
     * [ThumbInfo 视频缩略图信息]
     * @param string $thumbinfo  [description]
     * @param string $video_info [description]
     */
    public function ThumbInfo($thumbinfo = '',$video_info = ''){
        // 0 表示成功，1 表示等待处理，2 表示正在处理，3 表示处理失败，4 表示回调失败。
        switch ($thumbinfo['items'][0]['code']) {
            case 0:
                $video_info['error'] = '截图成功';
            break;
            case 1:
                $video_info['error'] = '等待截图';
            break;
            case 2:
                $video_info['error'] = '正在截图';
            break;
            case 3:
                $create_img_id = CreateVideoImg($video_info['video_key'], 'player');
                $this->db->where(array('video_id'=> $video_info['video_id']))->save(array('video_thumb_id'=> $create_img_id['id']));
                $video_info['error'] = '截图失败';
            break;
            case 4:
                $video_info['error'] = '截图回调失败';
            break;
            
            default:
                $video_info['error'] = '截图出现错误';
            break;
        }
        if ($thumbinfo['items'][0]['code'] == 0) {
            $video_info['thumb_path'] = isset($thumbinfo['items'][0]['key']) ? C('QINIU_DOMAIN') . $thumbinfo['items'][0]['key'] : '';
        }else{
            $video_info['message'] = $thumbinfo['items'][0]['error'];
        }
        return $video_info;
    }

    /**
     * [QueryVideo 请求视频转码信息]
     * @param [type] $data [description]
     */
    public function QueryVideo($data){
        foreach ($data as $key => $ret) {
            // 标清检测
            if (empty($ret['video_sd_key'])) {
                $data[$key]['video_sd_status']['zhuanma_info'] = $this->VideoZhuanma($ret['video_sd_id'], 'video_sd_id');
            }else{
                $data[$key]['video_sd_status']['zhuanma_info'] = '标清转码完成';
            }
            // 高清检测
            if (empty($ret['video_high_key'])) {
                $data[$key]['video_high_status']['zhuanma_info'] = $this->VideoZhuanma($ret['video_high_id'], 'video_high_id');
            }else{
                $data[$key]['video_high_status']['zhuanma_info'] = '高清转码完成';
            }
            // 超清
            if (empty($ret['video_super_key'])) {
                $data[$key]['video_super_status']['zhuanma_info'] = $this->VideoZhuanma($ret['video_super_id'], 'video_super_id');
            }else{
                $data[$key]['video_super_status']['zhuanma_info'] = '超清转码完成';
            }
        }
        // 返回数据
        return $data;
    }

    /**
     * [VideoZhuanma 查询七牛转码状态]
     * @param [type] $data [description]
     */
    public function VideoZhuanma($video_id, $video_key ,$video_id_name){
        $video_info[$video_id_name.'_status'] = VideoKeyStatus($video_id, 'player');
        // 状态码0成功，1等待处理，2正在处理，3处理失败，4通知提交失败。
        switch ($video_info[$video_id_name.'_status']['code']) {
            case 0:
                if (!DelVideo($video_key)) {
                    $data[$key]['video_sd_status']['ACTION_STATUS'] = '删除空间中的视频失败';
                }
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '标清转码完成';
                }elseif($video_id_name == 'video_high'){
                    return '高清转码完成';
                }else{
                    return '超清转码完成';
                }
            break;
            case 1:
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '等待标清转码';
                }elseif($video_id_name == 'video_high'){
                    return '等待高清转码';
                }else{
                    return '等待超清转码';
                }
            break;
            case 2:
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '正在标清转码';
                }elseif($video_id_name == 'video_high'){
                    return '正在高清转码';
                }else{
                    return '正在超清转码';
                }
            break;
            case 3:
                // 再次提交转码
                CreateVideoAvthumb($video_key, 'player');
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '标清转码失败';
                }elseif($video_id_name == 'video_high'){
                    return '高清转码失败';
                }else{
                    return '超清转码失败';
                }
            break;
            case 4:
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '提交标清转码失败';
                }elseif($video_id_name == 'video_high'){
                    return '提交标高转码失败';
                }else{
                    return '提交超清转码失败';
                }
            break;
            
            default:
                // 判定返回字符串
                if ($video_id_name == 'video_sd') {
                    return '标清转码出现错误';
                }elseif($video_id_name == 'video_high'){
                    return '高清转码出现错误';
                }else{
                    return '超清转码出现错误';
                }
            break;
        }
    }
}