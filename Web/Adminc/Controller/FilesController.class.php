<?php
namespace Adminc\Controller;
class FilesController extends BaseController {
    // 构造函数
	public function _initialize(){
		parent::_initialize();
	}

	/**
	 * [UploadOnePicture 上传单图片]
	 */
	public function UploadOnePicture(){
        /* 图片模型上传文件 */
        $Picture = D('Picture');
        $info = $Picture->UploadPicture($_FILES, C('PICTURE_UPLOAD'));
        /* 记录图片信息 */
        if($info){
        	$this->success('上传成功');
        } else {
        	$this->error($Picture->getError());
        }
	}
}