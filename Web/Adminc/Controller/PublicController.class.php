<?php
namespace Adminc\Controller;
use Think\Controller;
class PublicController extends Controller {

	// 构造函数
	public function _initialize(){
        // 定义未登录访问规则
        $action = array(
            'admin_out'
        );
        if (!in_array(ACTION_NAME, $action)) {
            if (isset($_SESSION['Adminc']['uid'])) {
                $this->error('你已经登录，正在跳转后台',U('Adminc/Index/index'));
                return false;
            }
        }
		$this->db = M('Adminc');
	}

	// 登录页面
    public function login(){
    	if (IS_POST) {
    		if (!$user_info = $this->db->where(array('user_name'=> $_POST['user_name']))->find()) {
    			$this->error('管理员不存在');
    		}
    		if ($user_info['user_pass'] != md5($_POST['user_pass'])) {
    			$this->error('管理员密码错误');
    		}
    		$_SESSION['Adminc']['uid'] = $user_info['uid'];
    		$this->success('恭喜你，登录成功',U('Adminc/Index/index'));
    	}else{
    		$this->display();
    	}
    }

    // 管理员登出
    public function admin_out(){
        if (empty($_SESSION['Adminc']['uid'])) {
            $this->error('你还未登录，请先登录',U('Adminc/Public/login'));
            return false;
        }
        unset($_SESSION['Adminc']['uid']);
        $this->success('登出成功',U('Adminc/Public/login'));
    }
}