<?php
namespace Adminc\Controller;
use Common\Util\Data;
class MenusController extends BaseController {

	/**
	 * [_initialize description]
	 * @return [type] [description]
	 */
	public function _initialize(){
		parent::_initialize();
		$this->db = D('Menus');
        $this->menus = S('Menus');
	}
	/**
	 * [index 菜单列表]
	 * @return [type] [description]
	 */
    public function index(){
    	$data = list_sort_by($this->menus,'sort');
    	$menus = Data::tree($data, 'title', 'id');
    	$this->assign('menus', $menus);
    	$this->display();
    }
    /**
     * [add 创建菜单]
     */
    public function add(){
    	if (IS_POST) {
    		if ($this->db->CreateMenu()) {
    			$this->success('菜单创建操作成功', U('index'));
    		}else{
    			$this->error($this->db->getError());
    		}
    	}else{
    		$menus = Data::tree($this->menus, 'title', 'id');
    		$this->assign('menus', $menus);
    		$this->display();
    	}
    }

    /**
     * [edit 修改菜单]
     * @return [type] [description]
     */
    public function edit(){
    	if (IS_POST) {
    		if ($this->db->EditMenu()) {
    			$this->success('菜单修改操作成功', U('index'));
    		}else{
    			$this->error($this->db->getError());
    		}
    	}else{
    		$id = intval($_GET['id']);
    		$menu = $this->db->where(array('id'=> $id))->find();
    		$menus = Data::tree($this->menus, 'title', 'id');
    		$this->assign('menus', $menus);
    		$this->assign('menu', $menu);
    		$this->display();
    	}
    }

    /**
     * [del 删除菜单]
     * @return [type] [description]
     */
    public function del(){
    	if ($this->db->DelMenus()) {
			$this->success('菜单删除操作成功', U('index'));
		}else{
			$this->error($this->db->getError());
		}
    }
    /**
     * [GetFile 更新菜单缓存]
     */
    public function GetFile(){
    	if ($this->db->GetFile()) {
			$this->success('菜单缓存更新成功', U('index'));
		}else{
			$this->error($this->db->getError());
		}
    }
}