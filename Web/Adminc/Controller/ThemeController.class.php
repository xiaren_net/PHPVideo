<?php
namespace Adminc\Controller;
use Common\Util\Dir;
use Common\Util\Xml;
/**
 * 模板管理
 * 楚羽幽<Name_Cyu@Foxmail.com>
 */
class ThemeController extends BaseController{

    /**
     * [_initialize description]
     * @return [type] [description]
     */
    public function _initialize(){
        parent::_initialize();
        $this->pc_theme = include "Data/Config/Theme.config.php";
        $this->pc_theme_path = "Data/Config/Theme.config.php";
    }
    /**
     * 模板列表视图
     */
    public function index(){
        $style = array();
        $dirs = Dir::tree('Theme/PC');
        foreach ($dirs as $tpl) {
            $xml = $tpl['path'] . 'config.xml';
            if (!is_file($xml)){
                continue;
            }
            if (!$config = Xml::toArray(file_get_contents($xml))){
                continue;
            }
            $tpl['name'] = isset($config['name']) ? $config['name'][0] : ''; // 模板名
            $tpl['author'] = isset($config['author']) ? $config['author'][0] : ''; // 作者
            $tpl['image'] = isset($config['image']) ? __ROOT__.'/Theme/PC/'.$tpl['filename'].'/'.$config['image'][0] : __PUBLIC__ . '/images/Theme_preview.jpg'; //预览图
            $tpl['email'] = isset($config['email']) ? $config['email'][0] : ''; // 邮箱
            $tpl['current'] = $this->pc_theme['DEFAULT_THEME'] == $tpl['filename'] ? 1 : 0; // 正在使用的模板
            $style[] = $tpl;
        }
        $this->assign('style', $style);
        $this->display();
    }

    /**
     * [style 操作更换主题]
     * @return [type] [description]
     */
    public function style(){
        $dir_name = I("dirName");
        if ($dir_name){
            // 操作更换主题
            $content = "<?php\nreturn " . var_export(array('DEFAULT_THEME'=> "{$dir_name}"),true) . ";";
            if (file_put_contents($this->pc_theme_path, $content)) {
                //删除前台编译文件
                is_dir("./Runtime/Cache/Home") and Dir::del("./Runtime/Cache/Home");
                $this->success('主题模版更换成功！');
            }else{
                $this->error('主题模版更换失败！');
            };
        }
    }
}