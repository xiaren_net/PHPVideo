<?php
namespace Adminc\Controller;
class IndexController extends BaseController {

	public function _initialize(){
		parent::_initialize();
	}

	// 后台首页
    public function index(){
        $this->display();
    }

    /**
     * [admin_info 管理员信息]
     * @return [type] [description]
     */
    public function admin_info(){
    	if (IS_POST) {
            $admins = M('Adminc');
            $uid = I('uid', 0, 'intval');
            if (empty($_POST['user_pass'])) {
                unset($_POST['user_pass']);
                $status = $admins->where(array('uid'=> $uid))->save($_POST);
                if ($status !== false) {
                    $this->success('操作成功');
                }else{
                    $this->success('操作失败');
                }
            }else{
                $_POST['user_pass'] = md5($_POST['user_pass']);
                $status = $admins->where(array('uid'=> $uid))->save($_POST);
                if ($status !== false) {
                    $this->success('操作成功');
                }else{
                    $this->success('操作失败');
                }
            }
        }else{
            $this->display();
        }
    }
}