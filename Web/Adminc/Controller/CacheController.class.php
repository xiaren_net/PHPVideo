<?php
/**
 * 全站缓存管理控制器
 * 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Adminc\Controller;
use Common\Util\Dir;
class CacheController extends BaseController{

	/**
	 * [index 缓存选项]
	 * @return [type] [description]
	 */
	public function index(){
		if (IS_POST) {
			// 删除缓存文件
			file_exists(RUNTIME_PATH . 'common~runtime.php') && unlink(RUNTIME_PATH . 'common~runtime.php');
			// 删除缓存文件夹
            Dir::del(RUNTIME_PATH . 'Temp/Data');
            Dir::del(RUNTIME_PATH . 'Temp/Cache');
            Dir::del(RUNTIME_PATH . 'Cache/Cache');

            // 赋值缓存
            S('CacheAction', $_POST['Action']);

            // 更新缓存动作
            $this->success('准备进行缓存更新...', U('options'),1);
		}else{
			$this->display();
		}
	}

	/**
	 * [options 选项缓存]
	 * @return [type] [description]
	 */
	public function options(){
		// 缓存读取
		$actionCache = S('CacheAction');

		// 判断缓存是否存在
		if ($actionCache) {
			// 遍历更新
			while($action = array_shift($actionCache)){
				switch ($action) {
					// 权限模块
					case 'auths':
						S('CacheAction',$actionCache);
						if (D('AuthRule')->GetFile()) {
							$this->success('权限缓存更新成功...', U('options'), 1);
							exit;
						}else{
							$this->error('权限缓存更新失败...', U('options'));
							exit;
						}
					break;
					// 前台菜单
					case 'menus':
						S('CacheAction',$actionCache);
						if (D('Menus')->GetFile()) {
							$this->success('前台菜单更新成功...', U('options'), 1);
							exit;
						}else{
							$this->error('前台菜单更新失败...', U('options'));
							exit;
						}
					break;
					// 视频分类
					case 'category':
						S('CacheAction',$actionCache);
						if (D('Category')->GetFile()) {
							$this->success('视频分类缓存更新成功...', U('options'), 1);
							exit;
						}else{
							$this->error('视频分类缓存更新失败...', U('options'));
							exit;
						}
					break;

					// 请选择缓存选项
					default:
						$this->error('请选择更新缓存选项！', U('index'), 3);
						exit;
					break;
				}
			}
		}else{
			S('CacheAction', null);
            $this->success('全部所选项缓存更新成功...', U('index'), 3);
            exit;
		}
	}
}