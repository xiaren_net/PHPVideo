<?php
/**
 * 贵州Mic工作室
 * 权限菜单规则表
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Controller;
use Common\Util\Data;
class MicruleController extends BaseController{

	/**
	 * [_initialize 构造方法]
	 * @return [type] [description]
	 */
	public function _initialize(){
		parent::_initialize();
		$this->db = D('AuthRule');
		$this->Auth_rule = S('Auth_rule');
	}

	/**
	 * [index 规则列表]
	 * @return [type] [description]
	 */
	public function index(){
		$data = Data::tree($this->Auth_rule, 'title', 'id', 'pid', 'icon-plus', 'text-blue');
		$this->assign('data', $data);
		$this->display();
	}

	/**
	 * [add 新增规则]
	 */
	public function add(){
		if (IS_POST) {
			if ($this->db->rule_add()) {
				$this->success('操作成功', U('index'));
			}else{
				$this->error($this->db->getError());
			}
		}else{
			$data = Data::tree($this->Auth_rule, 'title', 'id');
			$this->assign('data', $data);
			$this->display();
		}
	}

	/**
	 * [edit 编辑规则]
	 * @return [type] [description]
	 */
	public function edit(){
		if (IS_POST) {
			if ($this->db->rule_edit()) {
				$this->success('操作成功', U('index'));
			}else{
				$this->error($this->db->getError());
			}
		}else{
			$id = I('id', 0, 'intval');
			$field = $this->db->where(array('id'=> $id))->find();
			$arr = $this->db->order('id asc')->select();
			$data = Data::tree($arr, 'title', 'id');

			$this->assign('data', $data);
			$this->assign('field', $field);
			$this->display();
		}
	}

	/**
	 * [del 删除规则]
	 * @return [type] [description]
	 */
	public function del(){
		if ($this->db->del_rule()) {
			$this->success('操作成功', U('index'));
		}else{
			$this->error($this->db->getError());
		}
	}

	/**
	 * [GetFile 缓存规则]
	 */
	public function GetFile(){
		if ($this->db->GetFile()) {
			$this->success('操作成功');
		}else{
			$this->error('缓存更新失败或当前没有数据');
		}
	}
}