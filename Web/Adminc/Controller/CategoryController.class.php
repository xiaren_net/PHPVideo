<?php
namespace Adminc\Controller;
class CategoryController extends BaseController {

	// 构造函数
	public function _initialize(){
		parent::_initialize();
		$this->db = D('Category');
		$this->category = S('Category');
	}

	// 分类列表
    public function index(){
    	$this->assign('data', $this->category);
        $this->display();
    }
    // 分类添加
    public function add(){
    	if (IS_POST) {
    		if ($this->db->category_add()) {
    			$this->success('分类添加成功');
    		}else{
    			$this->error = '分类添加失败';
    			return false;
    		}
    	}else{
    		$this->display();
    	}
    }
    // 分类编辑
    public function edit(){
    	if (IS_POST) {
    		# code...
    	}else{
    		$this->display();
    	}
    }
    // 分类删除
    public function del(){
    }
    // 缓存分类
    public function GetFile(){
    	if ($this->db->GetFile()) {
    		$this->success('分类缓存成功');
    	}else{
    		$this->error($this->db->getError());
    	}
    }
}