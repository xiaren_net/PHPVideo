<?php
/**
 * 管理组列表
 * 贵州Mic工作室
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Controller;
use Common\Util\Data;
class GroupController extends BaseController{

    /**
     * [_initialize TP构造函数]
     * @return [type] [description]
     */
    public function _initialize(){
        parent::_initialize();
        $this->db = D('AuthGroup');
    }

    /**
     * [index 管理组列表]
     * @return [type] [description]
     */
    public function index(){
        $this->data = $this->db->select();
        $this->display();
    }

    /**
     * [add 新增管理组]
     */
    public function add(){
        if (IS_POST) {
            if ($this->db->admin_add()) {
                $this->success('操作成功', U('index'));
            }else{
                $this->error($this->db->getError());
            }
        }else{
            $this->display();
        }
    }

    /**
     * [edit 修改管理组]
     * @return [type] [description]
     */
    public function edit(){
        if (IS_POST) {
            if ($this->db->admin_edit()) {
                $this->success('操作成功', U('index'));
            }else{
                $this->error($this->db->getError());
            }
        }else{
            $id = I('id', '', 'intval');
            if (empty($id)) {
                $this->error('请传入参数ID');
            }
            $this->mic_field = $this->db->where(array('id'=> $id))->find();
            $this->display();
        }
    }

    /**
     * [del 删除管理组]
     * @return [type] [description]
     */
    public function del(){
        $id = I('id', '', 'intval');
        if (empty($id)) {
            $this->error('请传入参数ID');
        }
        if ($id == 1 OR $id == 2) {
            $this->error('无法删除系统管理组');
            return false;
        }
        if ($this->db->where(array('id'=> $id))->delete()) {
            M('AuthGroupAccess')->where(array('group_id'=> $id))->save(array('group_id'=> 2));
            $this->success('操作成功', U('index'));
        }else{
            $this->error('删除失败');
        }
    }


    /**
     * [mic_access 管理组授权]
     * @return [type] [description]
     */
    public function mic_access(){
        if (IS_POST) {
            if ($this->db->mic_access()) {
                $this->success('操作成功');
            }else{
                $this->error($this->db->getError());
            }
        }else{
            $id = I('id', '', 'intval');
            if (empty($id)) {
                $this->error('请传入参数ID');
            }
            $access = $this->db->where(array('id'=> $id))->field('rules')->find();
            $data = mic_rule(M('AuthRule')->select(), $access['rules']);
            $this->mic_data = Data::channelLevel($data, 0, "&nbsp;", 'id');
            $this->display();
        }
    }
}