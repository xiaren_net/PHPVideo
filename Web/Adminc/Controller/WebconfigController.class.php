<?php
namespace Adminc\Controller;
class WebconfigController extends BaseController {

	/**
	 * [_initialize description]
	 * @return [type] [description]
	 */
	public function _initialize(){
		parent::_initialize();
	}
	// 站点配置
    public function index(){
    	if (IS_POST) {
    		$configData = $_POST;
            //写入配置文件
            $content = "<?php\nreturn " . var_export($configData, true) . ";\n?>";
            if ($this->GetFile()) {
                if(file_put_contents('Data/Config/Web.config.php', $content)) {
                    $this->success('操作成功', U('index'));
                } else {
                    $this->error('操作失败', U('index'));
                }
            }
    	}else{
    		$this->display();
    	}
    }

    /**
     * [GetFile 更新站点数据]
     */
    public function GetFile(){
    	if (!is_file(RUNTIME_PATH . "Cache/Adminc")) {
            delDirAndFile(RUNTIME_PATH . "common~runtime.php", True);
            return true;
        }else{
            return true;
        }
    }
}