<?php
namespace Adminc\Controller;
use Think\Controller;
use Think\Auth;
class BaseController extends Controller {

	// 静态管理员数据
    public static $admin_info = NULL;

    // 构造方法
    public function _initialize(){
        // 设置用户uid
        $this->admin_user_id = $_SESSION['Adminc']['uid'];
        // 判断用户是否登录
        if (empty($this->admin_user_id)) {
            $this->error('您当前没有登录或登录超时', U('Public/login'));
            return;
        }
        // 实列化权限类 组装权限规则，并分配到页面上
        $this->mic_auth = new Auth();
        $this->mic_rule = MODULE_NAME . "/" . CONTROLLER_NAME . "/" . ACTION_NAME;

        // 不需要授权页面
        $this->sock_rule = array(
            'Adminc/Index/index',
            'Adminc/Index/info',
        );
        if (!in_array($this->mic_rule,$this->sock_rule)) {
            // 判断是否拥有访问权限
            if (!$this->mic_auth->check($this->mic_rule, $this->admin_user_id)) {
                $this->error('无效操作，你没有权限访问',U('Adminc/Index/index'));    
            }
        }
        // 跳转模块组
        if (ACTION_NAME == 'system') {
            $this->redirect(MODULE_NAME . '/' . CONTROLLER_NAME . '/index');
        }
        // 获取管理员信息
        $this->admin_info = $this->GetAdminInfo();

        // 分配组装域名
        $this->WEB_DOMAIN = dirname('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME']);

        // 获取菜单规则
        $this->MenuRule = $this->GetMenu();

        // 分配数据
        $this->action_title = '系统管理';

        // 组装权限规则
        $auth_rule = S('Auth_rule');
        $admin_nav = \Common\Util\Data::tree($auth_rule, 'title', 'id', 'pid', 'icon-plus', 'text-blue');
        // 分配面包屑
        $this->header1 = NULL;
        $this->header2 = NULL;
        foreach ($admin_nav as $key => $ret) {
            if (MODULE_NAME . "/" . CONTROLLER_NAME . '/system' == $ret['name']) {
                $this->header1 = $ret['title'];
            }
            if ($this->mic_rule == $ret['name']) {
                $this->header2 = $ret['title'];
            }
        }
    }

    /**
     * [GetMenu 获取菜单组]
     */
    public function GetMenu(){
        $data = S('Auth_rule');
        foreach ($data as $key => $ret) {
        	// 隐藏的权限删除
        	if ($ret['is_show'] == 0) {
                unset($data[$key]);
            }
            // 禁用的权限删除
            if ($ret['status'] == 0) {
                unset($data[$key]);
            }
            // 无权限的删除
            if (!$this->mic_auth->check($ret['name'], $this->admin_user_id)) {
            	unset($data[$key]);
            }
        }
        // 递归分类
        $MenuRule = \Common\Util\Data::channelLevel($data, 0, '&nbsp;', 'id');
        // 返回数据
        return $MenuRule;
    }

    /**
     * [GetAdminInfo 获取管理员信息]
     */
    public function GetAdminInfo(){
        // 管理员信息为空，进入获取
        if (is_null(self::$admin_info)) {
            // 获取并处理管理员信息
            $admin_info = M('Adminc')->where(array('user_id'=> $this->admin_user_id))->field('user_pass',true)->find();
            $admin_group = M('AuthGroupAccess')->where(array('user_id'=> $admin_info['uid']))->find();
            $admin_info['group'] = M('AuthGroup')->where(array('id'=> $admin_group['group_id']))->field('rules', true)->find();
            self::$admin_info = $admin_info;
        }
        // 分配管理员信息
        return self::$admin_info;
    }

    /**
     * 恢复条目
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    protected function resume (  $model , $where = array() , $msg = array( 'success'=>'状态恢复成功！', 'error'=>'状态恢复失败！')){
        $data    =  array('status' => 1);
        $this->editRow(   $model , $data, $where, $msg);
    }

    /**
     * 禁用条目
     * @param string $model 模型名称,供D函数使用的参数
     * @param array  $where 查询时的 where()方法的参数
     * @param array  $msg   执行正确和错误的消息,可以设置四个元素 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    protected function forbid ( $model , $where = array() , $msg = array( 'success'=>'状态禁用成功！', 'error'=>'状态禁用失败！')){
        $data    =  array('status' => 0);
        $this->editRow($model,$data, $where, $msg);
    }

    /**
     * 对数据表中的单行或多行记录执行修改 GET参数id为数字或逗号分隔的数字
     *
     * @param string $model 模型名称,供M函数使用的参数
     * @param array  $data  修改的数据
     * @param array  $where 查询时的where()方法的参数
     * @param array  $msg   执行正确和错误的消息 array('success'=>'','error'=>'', 'url'=>'','ajax'=>false)
     *                     url为跳转页面,ajax是否ajax方式(数字则为倒数计时秒数)
     *
     * @author 朱亚杰  <zhuyajie@topthink.net>
     */
    final protected function editRow ( $model ,$data, $where , $msg ){
        $id    = array_unique((array)I('id',0));
        $id    = is_array($id) ? implode(',',$id) : $id;
        $where = array_merge( array('id' => array('in', $id )) ,(array)$where );
        $msg   = array_merge( array( 'success'=>'操作成功！', 'error'=>'操作失败！', 'url'=>'' ,'ajax'=>IS_AJAX) , (array)$msg );
        if( M($model)->where($where)->save($data)!==false ) {
            $this->success($msg['success'],$msg['url'],$msg['ajax']);
        }else{
            $this->error($msg['error'],$msg['url'],$msg['ajax']);
        }
    }

    /**
     * [lists 通用分页列表数据集获取方法]
     * 可以通过url参数传递where条件,例如:  index.html?name=asdfasdfasdfddds
     * 可以通过url空值排序字段和方式,例如: index.html?_field=id&_order=asc
     * 可以通过url参数r指定每页数据条数,例如: index.html?r=5
     * @param  [type]  $model [模型名或模型实例]
     * @param  array   $where [where查询条件(优先级: $where>$_REQUEST>模型设定)]
     * @param  string  $order [排序条件,传入null时使用sql默认排序或模型属性(优先级最高);]
     * 请求参数中如果指定了_order和_field则据此排序(优先级第二);
     * 否则使用$order参数(如果$order参数,且模型也没有设定过order,则取主键降序);
     * @param  array   $base  [基本的查询条件]
     * @param  boolean $field [单表模型用不到该参数,要用在多表join时为field()方法指定参数]
     * @return [type]         [description]
     */
    protected function lists($model,$where=array(),$order='',$base = array('status'=>array('egt',0)),$field=true){
        $options    =   array();
        $REQUEST    =   (array)I('request.');
        if(is_string($model)){
            $model  =   M($model);
        }

        $OPT        =   new \ReflectionProperty($model,'options');
        $OPT->setAccessible(true);

        $pk         =   $model->getPk();
        if($order===null){
            //order置空
        }else if ( isset($REQUEST['_order']) && isset($REQUEST['_field']) && in_array(strtolower($REQUEST['_order']),array('desc','asc')) ) {
            $options['order'] = '`'.$REQUEST['_field'].'` '.$REQUEST['_order'];
        }elseif( $order==='' && empty($options['order']) && !empty($pk) ){
            $options['order'] = $pk.' desc';
        }elseif($order){
            $options['order'] = $order;
        }
        unset($REQUEST['_order'],$REQUEST['_field']);

        $options['where'] = array_filter(array_merge( (array)$base, /*$REQUEST,*/ (array)$where ),function($val){
            if($val===''||$val===null){
                return false;
            }else{
                return true;
            }
        });
        if( empty($options['where'])){
            unset($options['where']);
        }
        $options      =   array_merge( (array)$OPT->getValue($model), $options );
        $total        =   $model->where($options['where'])->count();

        if( isset($REQUEST['r']) ){
            $listRows = (int)$REQUEST['r'];
        }else{
            $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
        }
        $page = new \Think\Page($total, $listRows, $REQUEST);
        if($total>$listRows){
            $page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p =$page->show();
        $this->assign('_page', $p? $p: '');
        $this->assign('_total',$total);
        $options['limit'] = $page->firstRow.','.$page->listRows;

        $model->setProperty('options',$options);

        return $model->field($field)->select();
    }
}