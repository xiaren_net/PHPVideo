<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('add')}'" class="button radius-none">
						<i class="icon-edit"></i>
						添加管理员
					</button>
				<?php endif;?>
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-th-list"></i>
						管理组列表
					</button>
				<?php endif;?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->
	<table class="table table-bordered table-hover table-condensed">
		<tr class="bg">
			<td>序 号</td>
			<td>帐 号</td>
			<td>昵 称</td>
			<td>职 位</td>
			<td>状 态</td>
			<td>添加时间</td>
			<td>操 作</td>
		</tr>
		<volist name="data" id="evt">
			<tr>
				<td>{$evt.uid}</td>
				<td>{$evt.user_name}</td>
				<td>{$evt.nick_name}</td>
				<td>
					<volist name="evt['groups']" id="vo">
						{$vo.title}
					</volist>
				</td>
				<td>
					<if condition="$evt['status'] eq 1">
						<span class="text-main">正常</span>
					<else />
						<span class="text-red">锁定</span>
					</if>
				</td>
				<td>{$evt.addtime|date='Y-m-d H:i',###}</td>
				<td>
					<a href="{:U('edit', array('uid'=> $evt['uid']))}">
						<i class="icon-edit (alias)"></i>
						修 改
					</a>
					<a href="javascript:sendGet('{:U('del', array('uid'=> $evt['uid']))}', '是否确认删除该管理员？');">
						<i class="icon-times"></i>
						删 除
					</a>
				</td>
			</tr>
		</volist>
	</table>
</div>
<include file="Public/Footer" />