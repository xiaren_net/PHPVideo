<include file="Public/Header" />
	<div class="container-layout margin-top">
		<!-- 导航位置 -->
		<div class="text-small text-gray height-big">
			<div class="float-left height">
			</div>
			<div class="float-right">
				<div class="radius-rounded height">
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-th-list"></i>
						返回列表
					</button>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<hr />
		<!-- 导航位置 End -->
		<!-- Content Start -->
		<form action="{:U('add')}" method="post" class="form-auto" enctype="multipart/form-data">
			<div class="container-layout">
				<div class="line padding-top">
					<div class="x2 text-right padding-right padding-small-top">管理员账户：</div>
					<div class="x10">
						<input type="text" name="user_name" class="input radius-none" size="45" />
					</div>
				</div>
				<div class="line padding-top">
					<div class="x2 text-right padding-right padding-small-top">职位管理组：</div>
					<div class="x10">
						<select name="group_id" class="input">
							<option value="">=== 请选择管理员职位 ===</option>
							<volist name="data" id="vo">
								<option value="{$vo.id}">=== {$vo.title} ===</option>
							</volist>
						</select>
					</div>
				</div>
				<div class="line padding-top">
					<div class="x2 text-right padding-right padding-small-top">管理员状态：</div>
					<div class="x10">
						<label>
							<input type="radio" name="status" value="1" checked="checked" /> 正常
						</label>
						<label>
							<input type="radio" name="status" value="0" /> 锁定
						</label>
					</div>
				</div>
				<div class="line padding-top">
					<div class="x2 text-right padding-right padding-small-top">管理员密码：</div>
					<div class="x10">
						<input type="password" name="user_pass" class="input radius-none" size="45" />
					</div>
				</div>
				<div class="line padding-top">
					<div class="x2 text-right padding-right padding-small-top">管理员昵称：</div>
					<div class="x10">
						<input type="text" name="nick_name" class="input radius-none" size="45" />
					</div>
				</div>
				<div class="line padding-big-top">
					<div class="x2 text-right padding-right"></div>
					<div class="x3">
						<button type="submit" class="button bg-sub radius-none">提交确定</button>
					</div>
					<div class="x7 padding-left"></div>
				</div>
			</div>
		</form>
		<!-- Content END -->
	</div>
<include file="Public/Footer" />