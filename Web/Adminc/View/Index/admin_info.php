<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
			<span class="icon-home"></span>
			当前位置： 首页 -> {$header1} -> {$header2}
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->

	<!-- Content Start -->
	<div class="container-layout">
		<form id="node-add" class="form-auto">
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">管理账户：</div>
				<div class="x10 padding-small-top">
					{$admin_info.user_name}
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">管理员昵称：</div>
				<div class="x10">
					<input type="text" name="nick_name" value="{$admin_info.nick_name}" size="35" class="input" />
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">管理员密码：</div>
				<div class="x10">
					<input type="password" name="user_pass" placeholder="不填写，则不修改管理员密码." size="35" class="input" />
				</div>
			</div>
			<!-- <div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">最后登录IP：</div>
				<div class="x10 padding-small-top">
					{$admin_info.login_ip}
				</div>
			</div> -->
			<!-- <div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">最后登录：</div>
				<div class="x10 padding-small-top">
					{$admin_info.login_time|date='Y-m-d H:i:s',###}
				</div>
			</div> -->
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">注册日期：</div>
				<div class="x10 padding-small-top">
					{$admin_info.create_time|date='Y-m-d H:i:s',###}
				</div>
			</div>
			<div class="line padding-big-top">
				<div class="x2 text-right padding-right">
					<input type="hidden" name="uid" value="{$admin_info.uid}" />
				</div>
				<div class="x3">
					<button type="submit" class="button bg-sub radius-none">提交确定</button>
				</div>
				<div class="x7 padding-left"></div>
			</div>
		</form>
	</div>
	<!-- Content END -->
</div>
<script>
	$(function(){
		$('#node-add').submit(function(e){
			e.preventDefault();

			// 数据验证
			if ($(this).find('input[name=nickname]').val() =='') {
				layer.msg('请输入管理员昵称',{
					icon : 5,
					time : 3000
				});
				return false;
			};

			// 获取数据
			var dataform = $(this).serialize();

			// ajax
			$.ajax({
				type : 'post',
				url : '{:U(admin_info)}',
				data : dataform,
				dataType : "json",
				error : function(msg){
					alert(JSON.stringify(msg));
				},
				success : function(msg){
					if (msg.status == 1) {
						layer.msg(msg.info,{
							icon : 6,
							time : 3000
						},function(){
							window.location.href = msg.url;
						});
					}else{
						layer.msg(msg.info,{
							icon : 5,
							time : 3000
						});
					};
				},
			});
		});
	});
</script>
<include file="Public/Footer" />