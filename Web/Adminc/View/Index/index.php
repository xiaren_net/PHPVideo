<include file="Public/Header" />
<div class="container-layout">
	<div class="padding-left text-small height-big margin-top">
	    <div>
	      <div class="float-left ico-w-45"><img src="__PUBLIC__/images/ico_sun.png" width="32" height="32" /></div>
	      <div class="float-left">
	      		<strong>
		      		尊敬的
		      		<span class="text-red">
		      			{$admin_infonick_name}
		      		</span>
		      		<span class="timePd"></span>，
		      		欢迎使用
		      	</strong>
		      	{$Think.config.SYSTEM_WEBNAME}，
		      	系统版本：{$Think.config.SYSTEM_VERSION}
		      	更新时间：{$Think.config.SYSTEM_VERSION_TIME|date='Y年m月d日 H点i分',###}
	  		</div>
	      <div class="clearfix"></div>
	    </div>
	    <hr />
	    <div class="padding-top padding-bottom">
	      <div class="float-left ico-w-45"><img src="__PUBLIC__/images/ico_dp.png" width="32" height="32" /></div>
	      <div class="float-left">
	        <strong>温馨提示！</strong><br />
	        <ol class="list-unstyle height">
	          <li>您可以使用本系统对您的用户、视频、上传、编辑更多数据进行管理等。</li>
	          <li>请尽量使用 <span class="text-main">搜狗</span>丶<span class="text-red">火狐</span>丶<span class="text-sub">谷歌</span> 等兼容性较好浏览器访问本后台，将会看到更完美的后台操作界面</li>
	        </ol>
	      </div>
	      <div class="clearfix"></div>
	    </div>
	    <hr />
	    <!-- 网站基本数据 Start -->
	    <div class="padding-top padding-bottom">
	      <div class="float-left ico-w-45"><img src="__PUBLIC__/images/ico_tag.png" width="32" height="32" /></div>
	      <div class="float-left">
	        <strong>网站环境检测</strong><br />
	      </div>
	      <div class="clearfix"></div>
	    </div>
	    <hr />
	    <!-- 网站基本数据 End -->

	    <div class="line">
	      <div class="x4">
	        <!-- 程序团队 Start -->
	        <div class="padding-top">
	          <div class="float-left ico-w-45"><img src="__PUBLIC__/images/ico_user.png" width="32" height="32" /></div>
	          <div class="float-left">
	            <strong>程序团队</strong><br />
	            <p>
	              系统名称：{$Think.config.SYSTEM_WEBNAME}<br />
	              系统版本：{$Think.config.SYSTEM_VERSION}<br />
	              版权声明：{$Think.config.SYSTEM_COPYRIGHT}<br />
	              官方网站：<a href="{$Think.config.SYSTEM_DOMAIN}" target="_blank">{$Think.config.SYSTEM_DOMAIN}</a><br />
	              作者昵称：{$Think.config.SYSTEM_AUTHOR}<br />
	              作者QQ：{$Think.config.SYSTEM_QQ}<br />
	              E-mail：{$Think.config.SYSTEM_EMAIL}<br />
	            </p>
	          </div>
	        </div>
	        <!-- 程序团队 End -->
	      </div>
	      <div class="x8">
	        <!-- 程序说明 Start -->
	        <div class="padding-top padding-bottom">
	          <div class="float-left ico-w-45"><img src="__PUBLIC__/images/ico_02.png" width="32" height="32" /></div>
	          <div class="float-left">
	        <strong>网站程序说明</strong><br />
	         <ol class="height">
	          <li>本程序由 <span class="text-main">{$Think.config.SYSTEM_COPYRIGHT}</span> 所开发，提供视频建站使用。</li>
	          <li>请勿随意更改系统代码，否则 <span class="text-main">{$Think.config.SYSTEM_COPYRIGHT}</span> 不负任何责任！</li>
	          <li>尊重作者劳动成果，希望各站长在使用时不要修改版权和制作申明！</li>
	          <li>如需要更好的网站插件请联系我们 <span class="text-main">{$Think.config.SYSTEM_COPYRIGHT}</span> 订做。</li>
	          <li class="text-red">请做好数据备份，最大限度保障您的数据安全，感谢您的使用！</li>
	          <li>如果您想二次开发本程序，或者设计更精美的前台模板，<a href="http://document.16mic.com/" class="text-main" target="_blank">请点击这里查看手册</a></li>
	          </ol>
	      </div>
	          <div class="clearfix"></div>
	        </div>
	        <!-- 程序说明 End -->
	      </div>
	    </div>
	</div>
</div>
<include file="Public/Footer" />