<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
				<button class="button radius-none" onclick="GetUrl('{:U('add')}');">
					<i class="icon-edit"></i>
					添加分类
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/GetFile', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('GetFile')}');">
					<i class="icon-th-list"></i>
					缓存分类
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>序 号</td>
			<td>分类名称</td>
			<td>分类标题</td>
			<td>关键词</td>
			<td>创建时间</td>
			<td>操 作</td>
		</tr>
		<?php foreach ($data as $key => $ret): ?>
			<tr>
				<td><?php echo $ret['cate_id']; ?></td>
				<td><?php echo $ret['name']; ?></td>
				<td><?php echo $ret['title']; ?></td>
				<td><?php echo $ret['keywords']; ?></td>
				<td><?php echo friend_date($ret['create_time']); ?></td>
				<td>
					<a href="#">锁 定</a>
					<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/edit', $admin_user_id)): ?>
						<a href="<?php echo U('edit',array('uid'=> $ret['user_id']));?>">编 辑</a>
					<?php endif;?>
					<a href="<?php echo U('del',array('uid'=> $ret['user_id']));?>">删 除</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<include file="Public/Footer" />