<include file="Public/Header" />
<div class="container-fluid">
	<div class="container">
		<form action="{:U('add')}" method="post" class="form-inline">
			<div class="row">
				<div class="col-xs-3 text-right">
					层 级：
				</div>
				<div class="col-xs-6">
					<label>
						<select name="pid" class="form-control">
							<option value="0">=== 顶级分类 ===</option>
						</select>
					</label>
				</div>
				<div class="col-xs-3"></div>
			</div>
			<div class="row">
				<div class="col-xs-3 text-right">
					名 称：
				</div>
				<div class="col-xs-6">
					<label>
						<input type="text" name="name" class="form-control input-sm">
					</label>
				</div>
				<div class="col-xs-3"></div>
			</div>
			<div class="row">
				<div class="col-xs-3 text-right">
					标 题：
				</div>
				<div class="col-xs-6">
					<label>
						<input type="text" name="title" class="form-control input-sm">
					</label>
				</div>
				<div class="col-xs-3"></div>
			</div>
			<div class="row">
				<div class="col-xs-3 text-right">
					关键词：
				</div>
				<div class="col-xs-6">
					<label>
						<input type="text" name="keywords" class="form-control input-sm">
					</label>
				</div>
				<div class="col-xs-3"></div>
			</div>
			<div class="row">
				<div class="col-xs-3 text-right">
					描 述：
				</div>
				<div class="col-xs-6">
					<label>
						<textarea name="description" class="form-control input-sm" cols="30" rows="5"></textarea>
					</label>
				</div>
				<div class="col-xs-3"></div>
			</div>
			<div class="row">
				<div class="col-xs-3"></div>
				<div class="col-xs-6">
					<button type="submit" class="btn btn-primary">
						立即添加
					</button>
				</div>
				<div class="col-xs-3"></div>
			</div>
		</form>
	</div>
</div>
<include file="Public/Footer" />