<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>系统登录 - {$Think.config.SYSTEM_WEBNAME}</title>
	<Pintuer />
	<Layer />
	<link rel="stylesheet" href="__PUBLIC__/css/default.css">
	<script type="text/javascript" src="__PUBLIC__/js/default.js"></script>
</head>
<body class="bg-html-blue">
	<div class="container">
		<form id="login-form" class="form-auto">
			<div class="login-box padding-big-top padding-big-bottom">
				<div class="line margin-top">
					<div class="x4 text-right padding-right padding-small-top">
						账 号：
					</div>
					<div class="x8">
						<input type="text" class="input" size="35" name="user_name">
					</div>
				</div>
				<div class="line margin-top">
					<div class="x4 text-right padding-right padding-small-top">
						密 码：
					</div>
					<div class="x8">
						<input type="password" class="input" size="35" name="user_pass">
					</div>
				</div>
				<div class="line margin-top">
					<div class="x4">
					</div>
					<div class="x8">
						<button type="submit" class="button bg-green">
							立即登录
						</button>
						<button type="button" onclick="GetUrl('__ROOT__/');" class="button bg-white">
							返回首页
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
<script>
	$(function(){
		$('#login-form').submit(function(e){
			e.preventDefault();
			// 数据验证
			if ($(this).find('input[name=user_name]').val() == '') {
				layer.msg('请输入管理员登录账号');
				$(this).find('input[name=user_name]').focus();
				return false;
			};
			if ($(this).find('input[name=user_pass]').val() == '') {
				layer.msg('请输入管理员登录密码');
				$(this).find('input[name=user_pass]').focus();
				return false;
			};
			var data = $(this).serialize();
			// ajax
			$.ajax({
				url : "{:U('Adminc/Public/login')}",
				data : data,
				dataType : 'json',
				type : 'post',
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info,{
							icon : 5
						});
					};
				},
			});
		});
	});
</script>