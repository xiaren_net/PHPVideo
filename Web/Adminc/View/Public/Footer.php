<div class="footer-box">
	<div class="footer-fixed">
		欢迎使用
		<strong class="text-green">
			{$Think.config.SYSTEM_WEBNAME} {$Think.config.SYSTEM_VERSION}
		</strong>
		<span class="padding-left">
			<a href="{$Think.config.SYSTEM_DOMAIN}" target="_blank">
				<i class="icon-cloud-upload"></i>
				版本更新
			</a>
		</span>
		<span class="padding-left">
			最后更新：{$Think.config.SYSTEM_VERSION_TIME|date='Y年m月d日 H点i分',###}
		</span>
		<span class="padding-left">
			<a href="{$Think.config.SYSTEM_DOMAIN}" target="_blank">
				<i class="icon-home"></i>
				访问官网
			</a>
		</span>
	</div>
</div>
</body>
</html>