<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>{$action_title} - Powered by {$Think.config.SYSTEM_WEBNAME} {$Think.config.SYSTEM_VERSION}</title>
	<Pintuer />
	<Layer />
	<link rel="stylesheet" href="__PUBLIC__/css/default.css">
	<script type="text/javascript" src="__PUBLIC__/js/default.js"></script>
</head>
<body>
<div class="admin-top-box">
	<div class="line">
		<div class="x1 text-center">
			<a href="__ROOT__/" target="_blank">
				<img src="__PUBLIC__/images/logo.png" width="100%" height="60">
			</a>
		</div>
		<div class="x8">
			<ul class="admin-top-nav">
				<?php foreach ($MenuRule as $key => $ret): ?>
					<li>
						<a href="javascript:void(0);" onclick="GetUrl('{:U($ret['name'])}');">
							<?php echo $ret['title']; ?>
						</a>
						<?php if (!empty($ret['_data'])): ?>
							<ul class="admin-tow-list hidden">
								<?php foreach ($ret['_data'] as $k => $v): ?>
									<li>
										<a href="javascript:void(0);" onclick="GetUrl('{:U($v['name'])}');"><?php echo $v['title']; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="x3 text-right padding-right">
			<ul class="admin-top-nav">
				<li>
					<a href="javascript:void(0);">
						<i class="icon-user"></i>
						<?php echo $admin_info['nick_name']; ?>
					</a>
					<ul class="admin-tow-list hidden">
						<?php if ($mic_auth->check(MODULE_NAME . '/Index/admin_info', $admin_user_id)): ?>
							<li>
								<a href="javascript:void(0);" onclick="GetUrl('{:U('Index/admin_info')}');">我的资料</a>
							</li>
						<?php endif;?>
						<?php if ($mic_auth->check(MODULE_NAME . '/Public/admin_out', $admin_user_id)): ?>
							<li>
								<a href="javascript:void(0);" onclick="GetUrl('{:U('Public/admin_out')}');">立即退出</a>
							</li>
						<?php endif;?>
					</ul>
				</li>
				<?php if ($mic_auth->check(MODULE_NAME . '/Cache/index', $admin_user_id)): ?>
					<li>
						<a href="javascript:void(0);" onclick="GetUrl('{:U('Cache/index')}');">
							<i class="icon-rocket"></i>
							全站缓存
						</a>
					</li>
				<?php endif;?>
			</ul>
		</div>
	</div>
</div>