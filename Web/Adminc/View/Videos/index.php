<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					视频列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>序 号</td>
			<td>视频名称</td>
			<td>所属分类</td>
			<td>所属用户</td>
			<td>视频状态</td>
			<td>发布时间</td>
			<td>操作项目</td>
		</tr>
		<?php foreach ($data as $key => $ret): ?>
			<tr>
				<td><?php echo $ret['video_id']; ?></td>
				<td><?php echo $ret['video_title']; ?></td>
				<td><?php echo $ret['Category']['name']; ?></td>
				<td><?php echo $ret['Member']['nick_name']; ?></td>
				<td>
					<span class="padding-small-left">
						<i class="icon-desktop"></i>
						<?php echo $video_status[$ret['status']]; ?>
					</span>
					<span class="padding-small-left">
						<i class="icon-play-circle-o"></i>
						<?php echo $ret['video_sd_status']['zhuanma_info']; ?>
					</span>
					<span class="padding-small-left">
						<i class="icon-play-circle-o"></i>
						<?php echo $ret['video_high_status']['zhuanma_info']; ?>
					</span>
					<span class="padding-small-left">
						<i class="icon-play-circle-o"></i>
						<?php echo $ret['video_super_status']['zhuanma_info']; ?>
					</span>
				</td>
				<td><?php echo friend_date($ret['create_time']); ?></td>
				<td>
					<a href="<?php echo U('Home/Videos/info',array('id'=> $ret['video_id']));?>" target="_blank">
						<i class="icon-play-circle-o"></i>
						播 放
					</a>
					<a href="javascript:sendGet('{:U('shenhe',array('type'=> 'no','id'=> $ret['video_id']))}', '是否确认回收该视频的审核？');">
						<i class="icon-play-circle-o"></i>
						拒绝审核
					</a>
					<a href="<?php echo U('edit',array('id'=> $ret['video_id']));?>">
						<i class="icon-edit (alias)"></i>
						编 辑
					</a>
					<a href="<?php echo U('del',array('id'=> $ret['video_id']));?>">
						<i class="icon-bitbucket"></i>
						删 除
					</a>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="7" align="center">{$page}</td>
		</tr>
	</table>
</div>
<include file="Public/Footer" />