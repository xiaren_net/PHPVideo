<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					视频列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout">
	<form id="form-video-edit" class="form-auto">
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				视频分类：
			</div>
			<div class="x8">
				<select name="video_cid" class="input">
					<option value="0">=== 请选择视频分类 ===</option>
					<?php foreach ($category as $key => $ret): ?>
						<option value="<?php echo $ret['cate_id']; ?>" <?php if ($video_info['video_cid'] == $ret['cate_id']): ?> selected="selected" <?php endif; ?>>=== <?php echo $ret['name']; ?> ===</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				视频名称：
			</div>
			<div class="x8">
				<input type="text" name="video_title" value="{$video_info.video_title}" class="input" size="35" />
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				关键词：
			</div>
			<div class="x8">
				<input type="text" name="video_keywords" value="{$video_info.video_keywords}" class="input" size="35" />
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				所属用户：
			</div>
			<div class="x8 padding-small-top">
				{$video_info.Member.nick_name}
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				视频描述：
			</div>
			<div class="x8">
				<textarea name="video_description" class="input mic-resize" cols="35" rows="5">{$video_info.video_description}</textarea>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				视频封面：
			</div>
			<div class="x8">
				<?php if (empty($video_info['thumb_path'])): ?>
					<span class="text-red">
						系统正在为您自动截图中，请稍后按F5页面查看
					</span>
				<?php else: ?>
					<a href="javascript:void(0);" onclick="imgInfo();">
						<img src="{$video_info.thumb_path}" class="border radius padding-small" id="video_thumb" width="150" height="120">
					</a>
				<?php endif; ?>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				视频预览：
			</div>
			<div class="x8">
				<div id="a1"></div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4">
				<input type="hidden" name="id" value="{$video_info.video_id}">
			</div>
			<div class="x8">
				<button type="submit" class="button bg-green">
					立即发布
				</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	function imgInfo(){
		layer.open({
		  type: 1,
		  title: false,
		  closeBtn: 0,
		  area: ['450px', '400px'],
		  skin: 'layui-layer-nobg', //没有背景色
		  shadeClose: true,
		  content: "<img src='{$video_info.thumb_path}' width='450' height='400' />"
		});
	}
	$(function(){
		$('#form-video-edit').submit(function(e){
			e.preventDefault();

			var data = $(this).serialize();

			// ajax
			$.ajax({
				url : "{:U('edit')}",
				data : data,
				dataType : 'json',
				type : 'post',
				error : function(ret, err){
					alert(JSON.stringify(ret));
				},
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info);
					};
				},
			});
		});
		// CKplayer
		var flashvars={
			f:"{:C('QINIU_DOMAIN')}{$video_info.zhuanma_key}",
			c:0,
			b:1,
			// i:'http://www.ckplayer.com/static/images/cqdw.jpg'
			};
		var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};
		CKobject.embedSWF('__PUBLIC__/ckplayer/ckplayer.swf','a1','ckplayer_a1','400','300',flashvars,params);
		var video=['http://img.ksbbs.com/asset/Mon_1605/0ec8cc80112a2d6.mp4->video/mp4'];
		var support=['iPad','iPhone','ios','android+false','msie10+false'];
		CKobject.embedHTML5('a1','ckplayer_a1',400,300,video,flashvars,support);
		function closelights(){
			//关灯
			alert(' 本演示不支持开关灯');
		}
		function openlights(){
			//开灯
			alert(' 本演示不支持开关灯');
		}
	});
</script>
<script type="text/javascript" src="__PUBLIC__/ckplayer/ckplayer.js" charset="utf-8"></script>
<include file="Public/Footer" />