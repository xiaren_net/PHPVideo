<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('add')}'" class="button radius-none">
						<i class="icon-edit"></i>
						添加管理组
					</button>
				<?php endif;?>
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-th-list"></i>
						管理组列表
					</button>
				<?php endif;?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->
	<table class="table table-bordered table-hover table-condensed">
		<tr class="bg">
			<td>序 号</td>
			<td>管理组名称</td>
			<td>状 态</td>
			<td>添加时间</td>
			<td>操 作</td>
		</tr>
		<volist name="data" id="evt">
			<tr>
				<td>{$evt.id}</td>
				<td>{$evt.title}</td>
				<td>
					<?php if ($evt['status'] == 1): ?>
						<span class="text-green">启用</span>
						<?php else: ?>
						<span class="text-red">禁用</span>
					<?php endif ?>
				</td>
				<td>{$evt.addtime|date='Y-m-d H:i',###}</td>
				<td>
					<a href="{:U('mic_access', array('id'=> $evt['id']))}">
						<i class="icon-magic"></i>
						授 权
					</a>
					<a href="{:U('edit', array('id'=> $evt['id']))}">
						<i class="icon-edit (alias)"></i>
						修 改
					</a>
					<?php if ($evt['id'] == 1 OR $evt['id'] == 2): ?>
						<a href="javascript:;" class="text-gray" title="系统管理员组，禁止删除">
							<i class="icon-times"></i>
							删 除
						</a>
						<?php else: ?>
						<a href="javascript:sendGet('{:U('del', array('id'=> $evt['id']))}', '是否确认删除该管理组？');">
							<i class="icon-times"></i>
							删 除
						</a>
					<?php endif;?>
				</td>
			</tr>
		</volist>
	</table>
</div>
<include file="Public/Footer" />