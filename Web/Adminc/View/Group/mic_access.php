<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-th-list"></i>
						管理组列表
					</button>
				<?php endif;?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->
</div>
<!-- 权限列表 开始 -->
<div class="container-layout">
	<div class="line border bg padding-top padding-bottom padding-big-left">
		<i class="icon-cube"></i> 菜单模块
		<i class="icon-cubes margin-left"></i> 功能模块
		<i class="icon-edit (alias) margin-left"></i> 操作
	</div>
	<form action="" method="post" class="form-auto">
		<?php foreach ($mic_data as $key1 => $evt1): ?>
			<!-- 块级菜单项 开始 -->
			<div class="line margin-top border padding-bottom">
				<label>
					<h4 class="bg padding-big-left padding-small-bottom padding-top border-bottom cursor-pointer">
						<input type="checkbox" level="1" name="access[]" <?php if ($evt1['access'] == 1): ?>checked="checked"<?php endif;?> value="<?php echo $evt1['id'];?>" /> <i class="icon-cube"></i> <?php echo $evt1['title'];?>
					</h4>
				</label>
				<ul class="list-inline margin-top">
				<?php foreach ($evt1['_data'] as $key2 => $evt2): ?>
					<li class="padding-left-none">
						<label class="cursor-pointer">
							<input type="checkbox" level="1" name="access[]" <?php if ($evt2['access'] == 1): ?>checked="checked"<?php endif;?> value="<?php echo $evt2['id'];?>" />
							<i class="icon-cubes margin-left"></i>
							<?php echo $evt2['title'];?>
						</label>
						<ul class="list-inline margin-top">
						<?php foreach ($evt2['_data'] as $key3 => $evt3): ?>
							<li class="padding-left-none">
								<label class="cursor-pointer">
									<input type="checkbox" level="3" name="access[]" <?php if ($evt3['access'] == 1): ?>checked="checked"<?php endif;?> value="<?php echo $evt3['id'];?>" /> <i class="icon-edit (alias) margin-left"></i> <?php echo $evt3['title'];?>
								</label>
								<ul class="list-inline margin-top">
								<?php foreach ($evt3['_data'] as $key4 => $evt4): ?>
									<li class="padding-left-none">
										<label class="cursor-pointer">
											<input type="checkbox" level="3" name="access[]" <?php if ($evt4['access'] == 1): ?>checked="checked"<?php endif;?> value="<?php echo $evt4['id'];?>" />  <i class="icon-edit (alias) margin-left"></i> <?php echo $evt4['title'];?>
										</label>
									</li>
								<?php endforeach;?>
								</ul>
							</li>
						<?php endforeach;?>
						</ul>
					</li>
				<?php endforeach;?>
				</ul>
			</div>
			<!-- 块级菜单项 结束 -->
		<?php endforeach;?>
		<div class="line margin-top border text-center padding-top padding-bottom">
			<input type="hidden" name="uid" value="<?php echo $_GET['id'];?>" />
			<button type="submit" class="button bg-sub radius-none">
				<i class="icon-check-square-o"></i>
				确认授权
			</button>
		</div>
	</form>
</div>
<!-- 权限列表 结束 -->
<include file="Public/Footer" />