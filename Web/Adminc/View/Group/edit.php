<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
					<i class="icon-th-list"></i>
					返回列表
				</button>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->

	<!-- Content Start -->
	<form action="{:U('edit')}" method="post" class="form-auto" enctype="multipart/form-data">
		<div class="container-layout">
			<div class="line padding-top">
				<div class="x2 text-right padding-right">管理组名称</div>
				<div class="x10">
					<input type="text" name="title" value="{$mic_field.title}" class="input radius-none" size="45" />
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">管理组状态</div>
				<div class="x10">
					<label>
						<input type="radio" name="status" value="1" <?php if ($mic_field['status'] == 1): ?>checked="checked"<?php endif;?> /> 启用
					</label>
					<label>
						<input type="radio" name="status" value="0" <?php if ($mic_field['status'] == 0): ?>checked="checked"<?php endif;?> /> 禁用
					</label>
				</div>
			</div>
			<div class="line padding-big-top">
				<div class="x2 text-right padding-right">
					<input type="hidden" name="id" value="{$mic_field.id}" />
				</div>
				<div class="x3">
					<button type="submit" class="button bg-sub radius-none">提交确定</button>
				</div>
				<div class="x7 padding-left"></div>
			</div>
		</div>
	</form>
	<!-- Content END -->
</div>
<include file="Public/Footer" />