<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
				<button class="button radius-none" onclick="GetUrl('{:U('add')}');">
					<i class="icon-edit"></i>
					创建菜单
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/GetFile', $admin_user_id)): ?>
				<button class="button bg-sub radius-none" onclick="GetUrl('{:U('GetFile')}');">
					<i class="icon-magic"></i>
					更新缓存
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					菜单列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>菜单连接</td>
			<td>菜单名称</td>
			<td>菜单排序</td>
			<td>菜单状态</td>
			<td>创建时间</td>
			<td>操作项目</td>
		</tr>
		<?php foreach ($menus as $key => $ret): ?>
			<tr>
				<td><?php echo $ret['url']; ?></td>
				<td>
					<div style="text-indent:<?php if ($ret['_level'] > 1): ?>{$ret['_level'] * 10}<?php else: ?>0<?php endif;?>px;">{$ret._name}</div>
				</td>
				<td><?php echo $ret['sort']; ?></td>
				<td>
					<?php if ($ret['status'] == 1): ?>
						<span class="text-main">启用</span>
					<?php else: ?>
						<span class="text-red">禁用</span>
					<?php endif; ?>
				</td>
				<td><?php echo friend_date($ret['create_time']); ?></td>
				<td>
					<a href="{:U('add', array('id'=> $ret['id']))}">
						<i class="icon-external-link"></i>
						添加子菜单
					</a>
					<a href="{:U('edit', array('id'=> $ret['id']))}">
						<i class="icon-edit (alias)"></i>
						编 辑
					</a>
					<a href="javascript:sendGet('{:U('del', array('id'=> $ret['id']))}', '是否确认删除该菜单？');">
						<i class="icon-bitbucket"></i>
						删 除
					</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<include file="Public/Footer" />