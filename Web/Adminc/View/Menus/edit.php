<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					菜单列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<form id="form-menus-edit" class="form-auto">
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				菜单等级：
			</div>
			<div class="x8">
				<select name="pid" class="input radius-none">
					<option value="0">=== 顶级菜单 ===</option>
					<volist name="menus" id="vo">
						<option value="{$vo.id}" <?php if ($vo['id'] == $menu['pid']): ?> selected="selected" <?php endif;?>>=== {$vo._name} ===</option>
					</volist>
				</select>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				菜单名称：
			</div>
			<div class="x8">
				<input type="text" name="title" value="{$menu.title}" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				菜单连接：
			</div>
			<div class="x8">
				<input type="text" name="url" value="{$menu.url}" size="35" class="input text-block">
				<label class="text-block padding-small-top text-gray">
					温馨提示：带有htpp://的连接则是外部连接<br />否则这是使用大U方法生成的站内连接
				</label>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				菜单状态：
			</div>
			<div class="x8 padding-small-top">
				<label>
					<input type="radio" name="status" value="1"<if condition="$menu['status'] eq 1"> checked="checked" </if>/>
					启用
				</label>
				<label>
					<input type="radio" name="status" value="0"<if condition="$menu['status'] eq 0"> checked="checked" </if>/>
					禁用
				</label>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				菜单排序：
			</div>
			<div class="x8">
				<input type="text" name="sort" value="{$menu.sort}" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4">
				<input type="hidden" name="id" value="{$menu.id}">
			</div>
			<div class="x8">
				<button type="submit" class="button bg-sub radius-none">
					<i class="icon-edit"></i>
					确认修改
				</button>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
		$('#form-menus-edit').submit(function(e){
			e.preventDefault();
			// 数据验证
			if ($(this).find('input[name=title]').val() == '') {
				layer.msg('请输入菜单吗名称');
				$(this).find('input[name=title]').focus();
				return false;
			};
			if ($(this).find('input[name=url]').val() == '') {
				layer.msg('请输入菜单连接');
				$(this).find('input[name=url]').focus();
				return false;
			};
			// 获取表单数据
			var data = $(this).serialize();

			// ajax
			$.ajax({
				url : "{:U('edit')}",
				data : data,
				dataType : "json",
				type : 'post',
				error : function(ret, err){
					alert(JSON.stringify(ret));
				},
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info);
					};
				},
			});
		});
	});
</script>
<include file="Public/Footer" />