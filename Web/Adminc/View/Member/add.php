<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					用户列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<form id="form-member-add" class="form-auto">
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				手 机：
			</div>
			<div class="x8">
				<input type="text" name="mobile" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				昵 称：
			</div>
			<div class="x8">
				<input type="text" name="nick_name" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				密 码：
			</div>
			<div class="x8">
				<input type="password" name="user_pass" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				确认密码：
			</div>
			<div class="x8">
				<input type="password" name="user_pass_confirm" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4"></div>
			<div class="x8">
				<button type="submit" class="button bg-sub radius-none">
					<i class="icon-edit"></i>
					立即添加
				</button>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
		$('#form-member-add').submit(function(e){
			e.preventDefault();
			// 数据验证
			if ($(this).find('input[name=mobile]').val() == '') {
				layer.msg('请输入手机号码');
				$(this).find('input[name=mobile]').focus();
				return false;
			};
			if ($(this).find('input[name=nick_name]').val() == '') {
				layer.msg('请输入用户昵称');
				$(this).find('input[name=nick_name]').focus();
				return false;
			};
			if ($(this).find('input[name=user_pass]').val() == '') {
				layer.msg('请输入用户密码');
				$(this).find('input[name=user_pass]').focus();
				return false;
			};
			if ($(this).find('input[name=user_pass_confirm]').val() == '') {
				layer.msg('请输入确认密码');
				$(this).find('input[name=user_pass_confirm]').focus();
				return false;
			};
			if ($(this).find('input[name=user_pass]').val() != $(this).find('input[name=user_pass_confirm]').val()) {
				layer.msg('用户密码与确认密码不一致');
				return false;
			};
			// 获取表单数据
			var data = $(this).serialize();

			// ajax
			$.ajax({
				url : "{:U('add')}",
				data : data,
				dataType : "json",
				type : 'post',
				error : function(ret, err){
					alert(JSON.stringify(ret));
				},
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info);
					};
				},
			});
		});
	});
</script>
<include file="Public/Footer" />