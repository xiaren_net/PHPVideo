<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
				<button class="button radius-none" onclick="GetUrl('{:U('add')}');">
					<i class="icon-edit"></i>
					添加用户
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-sub radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					用户列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>序 号</td>
			<td>手机号码</td>
			<td>用户昵称</td>
			<td>注册时间</td>
			<td>操作项目</td>
		</tr>
		<?php foreach ($data as $key => $ret): ?>
			<tr>
				<td><?php echo $ret['user_id']; ?></td>
				<td><?php echo $ret['mobile']; ?></td>
				<td><?php echo $ret['nick_name']; ?></td>
				<td><?php echo friend_date($ret['create_time']); ?></td>
				<td>
					<a href="#">
						<i class="icon-unlock-alt"></i>
						锁 定
					</a>
					<a href="<?php echo U('edit',array('uid'=> $ret['user_id']));?>">
						<i class="icon-edit (alias)"></i>
						编 辑
					</a>
					<a href="<?php echo U('del',array('uid'=> $ret['user_id']));?>">
						<i class="icon-bitbucket"></i>
						删 除
					</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<include file="Public/Footer" />