<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					用户列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<form id="form-member-edit" class="form-auto">
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				手 机：
			</div>
			<div class="x8 padding-small-top">
				{$user_info.mobile}
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				昵 称：
			</div>
			<div class="x8">
				<input type="text" name="nick_name" value="{$user_info.nick_name}" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				密 码：
			</div>
			<div class="x8">
				<input type="password" name="user_pass" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-small-top padding-small-right">
				确认密码：
			</div>
			<div class="x8">
				<input type="password" name="user_pass_confirm" size="35" class="input">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4">
				<input type="hidden" name="user_id" value="{$user_info.user_id}" />
			</div>
			<div class="x8">
				<button type="submit" class="button bg-sub radius-none">
					<i class="icon-edit"></i>
					立即修改
				</button>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
		$('#form-member-edit').submit(function(e){
			e.preventDefault();
			// 数据验证
			if ($(this).find('input[name=nick_name]').val() == '') {
				layer.msg('请输入用户昵称');
				$(this).find('input[name=nick_name]').focus();
				return false;
			};
			// 获取表单数据
			var data = $(this).serialize();

			// ajax
			$.ajax({
				url : "{:U('edit')}",
				data : data,
				dataType : "json",
				type : 'post',
				error : function(ret, err){
					alert(JSON.stringify(ret));
				},
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info);
					};
				},
			});
		});
	});
</script>
<include file="Public/Footer" />