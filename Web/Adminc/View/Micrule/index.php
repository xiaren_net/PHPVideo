<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/add', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('add')}'" class="button radius-none">
						<i class="icon-edit"></i>
						添加权限
					</button>
				<?php endif;?>
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/GetFile', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('GetFile')}'" class="button bg-sub radius-none">
						<i class="icon-magic"></i>
						更新缓存
					</button>
				<?php endif;?>
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-th-list"></i>
						菜单列表
					</button>
				<?php endif;?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->
	<table class="table table-bordered table-hover table-condensed">
		<tr class="bg">
			<td>序 号</td>
			<td>规 则</td>
			<td>名 称</td>
			<td>状 态</td>
			<td>隐 藏</td>
			<td>排 序</td>
			<td>条 件</td>
			<td>添加时间</td>
			<td>操 作</td>
		</tr>
		<volist name="data" id="evt">
			<tr <if condition="$evt['_level'] NEQ 1 OR $evt['_level'] EGT 3">class="mic-hidden" level="level_{$evt['pid']}"  title="点击展开下级分类"</if>>
				<td>{$evt.id}</td>
				<td>{$evt.name}</td>
				<td onclick="showRule(this,'level_{$evt['id']}');" class="cursor-pointer" <if condition="$evt['_level'] EQ 1">title="点击展开下级分类" </if>>
					<div style="text-indent:<?php if ($evt['_level'] > 1): ?>{$evt['_level'] * 10}<?php else: ?>0<?php endif;?>px;">{$evt._name}</div>
				</td>
				<td>
					<?php if ($evt['status'] == 1): ?>
						<span class="text-green">启用</span>
						<?php else: ?>
						<span class="text-red">禁用</span>
					<?php endif ?>
				</td>
				<td>
					<?php if ($evt['is_show'] == 1): ?>
						<span class="text-green">显示</span>
						<?php else: ?>
						<span class="text-red">隐藏</span>
					<?php endif ?>
				</td>
				<td>{$evt.sort}</td>
				<td>{$evt.condition}</td>
				<td>{$evt.create_time|date='Y-m-d H:i',###}</td>
				<td>
					<a href="{:U('add', array('id'=> $evt['id']))}">
						<i class="icon-external-link"></i>
						添加子权限
					</a>
					<a href="{:U('edit', array('id'=> $evt['id']))}">
						<i class="icon-edit (alias)"></i>
						修 改
					</a>
					<a href="javascript:sendGet('{:U('del', array('id'=> $evt['id']))}', '删除该菜单，也会把下级菜单删除<br />是否确认删除该菜单规则？');">
						<i class="icon-times"></i>
						删 除
					</a>
				</td>
			</tr>
		</volist>
	</table>
</div>
<script type="text/javascript">
	/**
	 * [showRule description]
	 * @param  {[type]} obj   [description]
	 * @param  {[type]} level [description]
	 * @return {[type]}       [description]
	 */
	function showRule(obj,attrs){
		if ($('tr[level='+attrs+']').is(':hidden')) {
			$('tr[level='+attrs+']').show(500);
		}else{
			$('tr[level='+attrs+']').hide(500);
		};
	}
</script>
<include file="Public/Footer" />