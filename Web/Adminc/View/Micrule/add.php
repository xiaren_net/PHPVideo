<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
					<i class="icon-th-list"></i>
					返回列表
				</button>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->

	<!-- Content Start -->
	<form action="{:U('add')}" method="post" class="form-auto" enctype="multipart/form-data">
		<div class="container-layout">
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">上级权限</div>
				<div class="x10">
					<select name="pid" class="input radius-none container-width-200">
						<option value="0">=== 顶级权限菜单 ===</option>
						<volist name="data" id="vo">
							<option value="{$vo.id}" <?php if ($vo['id'] == $_GET['id']): ?>selected="selected"<?php endif;?>>=== {$vo._name} ===</option>
						</volist>
					</select>
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">权限名称</div>
				<div class="x10">
					<input type="text" name="title" class="input radius-none" size="45" />
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">权限规则</div>
				<div class="x10">
					<input type="text" name="name" class="input radius-none" size="45" />
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">权限状态</div>
				<div class="x10">
					<label><input type="radio" name="status" value="1" checked="checked"/> 启用</label>
					<label><input type="radio" name="status" value="0" /> 禁用</label>
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">是否显示</div>
				<div class="x10">
					<label><input type="radio" name="is_show" value="1" checked="checked"/> 显示</label>
					<label><input type="radio" name="is_show" value="0" /> 隐藏</label>
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">权限条件</div>
				<div class="x10">
					<input type="text" name="condition" placeholder="可选条件，可不填写" class="input radius-none" size="45"/>
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">图标代码</div>
				<div class="x10">
					<input type="text" name="node_ico" class="input radius-none" size="45"/>
				</div>
			</div>
			<div class="line padding-top">
				<div class="x2 text-right padding-right">排 序</div>
				<div class="x10">
					<input type="text" name="sort" class="input radius-none" value="0" size="45"/>
				</div>
			</div>
			<div class="line padding-big-top">
				<div class="x2 text-right padding-right"></div>
				<div class="x3">
					<button type="submit" class="button bg-sub radius-none">提交确定</button>
				</div>
				<div class="x7 padding-left"></div>
			</div>
		</div>
	</form>
	<!-- Content END -->
</div>
<include file="Public/Footer" />