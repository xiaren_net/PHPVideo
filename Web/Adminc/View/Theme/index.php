<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- 导航位置 -->
	<div class="text-small text-gray height-big">
		<div class="float-left height">
		</div>
		<div class="float-right">
			<div class="radius-rounded height">
				<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
					<button type="button" onClick="location.href='{:U('index')}'" class="button bg-main radius-none">
						<i class="icon-list"></i>
						主题列表
					</button>
				<?php endif;?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<hr />
	<!-- 导航位置 End -->
	<!-- Content Start -->
	<div class="line text-center">
		<?php foreach ($style as $key => $ret): ?>
			<div class="x2 padding">
				<div class="border padding">
					<img src="<?php echo $ret['image']; ?>" width="230" height="260" class="padding border margin-top">
					<div class="margin-small-top line">
						<div class="x4 text-right">
							主题名称：
						</div>
						<div class="x8 text-left">
							<?php echo $ret['name']; ?>
						</div>
					</div>
					<div class="margin-small-top line">
						<div class="x4 text-right">
							主题目录：
						</div>
						<div class="x8 text-left">
							<?php echo $ret['filename']; ?>
						</div>
					</div>
					<div class="margin-small-top line">
						<div class="x4 text-right">
							作者昵称：
						</div>
						<div class="x8 text-left">
							<?php echo $ret['author']; ?>
						</div>
					</div>
					<div class="margin-small-top line">
						<div class="x4 text-right">
							作者邮箱：
						</div>
						<div class="x8 text-left">
							<?php echo $ret['email']; ?>
						</div>
					</div>
					<div class="margin-top">
						<?php if ($ret['current'] == 1): ?>
							<button type="button" onclick="tis_theme();" class="button bg-red radius-none">
								主题正在使用
							</button>
							<?php else: ?>
							<button type="button" onclick="on_style('<?php echo U('style',array('dirName'=> $ret['filename']));?>');" class="button bg-blue radius-none">
								点击使用主题
							</button>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<!-- Content End -->
</div>
<script>
	/**
	 * [on_style 切换模版]
	 * @param  {[type]} path [description]
	 * @return {[type]}      [description]
	 */
	function on_style(path){
		layer.confirm('是否确认切换该主题风格？',function(){
			$.getJSON(path,function(ret,err){
				if (ret.status == 1) {
					layer.msg(ret.info,{
						icon : 6,
						time : 2000
					},function(){
						window.location.href=ret.url;
					});
				}else{
					layer.msg(ret.info,{
						icon : 5,
						time : 3000,
					});
				};
			});
		});
	}
	/**
	 * [tis_theme 模版提示]
	 * @return {[type]} [description]
	 */
	function tis_theme(){
		layer.msg('该模版已经正在使用中...');
		return false;
	}
</script>
<include file="Public/Footer" />