<include file="Public/Header" />
<div class="container-layout margin-large-top">
	<form id="form-webconfig" class="form-auto">
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				站点名称：
			</div>
			<div class="x8">
				<input type="text" name="MIC_WEB_NAME" value="{:C('MIC_WEB_NAME')}" class="input" size="35">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				站点域名：
			</div>
			<div class="x8">
				<input type="text" name="MIC_DOMAIN" value="{:C('MIC_DOMAIN')}" class="input" size="35">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				站点标题：
			</div>
			<div class="x8">
				<input type="text" name="MIC_TITLE" value="{:C('MIC_TITLE')}" class="input" size="35">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				关键词：
			</div>
			<div class="x8">
				<input type="text" name="MIC_KEYWORDS" value="{:C('MIC_KEYWORDS')}" class="input" size="35">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				站点描述：
			</div>
			<div class="x8">
				<textarea name="MIC_DESCRIPTION" class="input mic-resize" cols="35" rows="5">{:C('MIC_DESCRIPTION')}</textarea>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4 text-right padding-right padding-small-top">
				加密KEY：
			</div>
			<div class="x8">
				<input type="text" name="MIC_TOKEN_KEY" value="{:C('MIC_TOKEN_KEY')}" class="input" size="35">
			</div>
		</div>
		<div class="line margin-top">
			<div class="x4"></div>
			<div class="x8">
				<button type="submit" class="button bg-green">
					提交配置
				</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(function(){
		$('#form-webconfig').submit(function(e){
			e.preventDefault();
			var data = $(this).serialize();
			$.ajax({
				url : "{:U('Webconfig/index')}",
				data : data,
				dataType : 'json',
				type : 'post',
				error : function(ret, err){
					alert(JSON.stringify(ret));
				},
				success : function(ret){
					if (ret.status == 1) {
						layer.msg(ret.info,{
							icon : 6,
							time : 1000
						},function(){
							window.location.href=ret.url;
						});
					}else{
						layer.msg(ret.info);
					};
				},
			});
		});
	});
</script>
<include file="Public/Footer" />