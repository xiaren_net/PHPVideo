<include file="Public/Header" />
<div class="container-layout margin-top">
	<!-- Content Start -->
	<div class="container-layout">
		<form action="{:U('index')}" method="post">
			<div class="line padding-top">
				<div class="x2 text-right padding-right padding-small-top">缓存选项：</div>
				<div class="x10 padding-small-top">
					<ul class="list-unstyle height">
						<li>
							<label class="padding-left"><input type="checkbox" name="Action[]" checked="checked" value="auths" /> 权限菜单</label>
							<label class="padding-left"><input type="checkbox" name="Action[]" checked="checked" value="menus" /> 前台菜单</label>
							<label class="padding-left"><input type="checkbox" name="Action[]" checked="checked" value="category" /> 视频分类</label>
						</li>
					</ul>
				</div>
			</div>
			<div class="line padding-big-top">
				<div class="x2 text-right padding-right"></div>
				<div class="x3">
					<button type="submit" class="button bg-sub radius-none">
						<i class="icon-magic"></i>
						更新缓存
					</button>
				</div>
				<div class="x7 padding-left"></div>
			</div>
		</form>
	</div>
	<!-- Content END -->
</div>
<include file="Public/Footer" />