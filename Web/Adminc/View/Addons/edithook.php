<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/hooks', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('hooks')}');">
					<i class="icon-list"></i>
					钩子列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<form action="{:U('updateHook')}" method="post" class="form-auto">
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<strong>钩子名称</strong>
			<span class="margin-left text-gray">(需要在程序中先添加钩子，否则无效)</span>
		</div>
	</div>
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<input type="text" value="{$data.name}" name="name" class="input radius-none" size="45">
		</div>
	</div>
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<strong>钩子描述</strong>
			<span class="margin-left text-gray">(钩子的描述信息)</span>
		</div>
	</div>
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<textarea name="description" class="input radius-none mic-resize" cols="45" rows="5">{$data.description}</textarea
		</div>
	</div>
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<strong>钩子类型</strong>
			<span class="margin-left text-gray">(区分钩子的主要用途)</span>
		</div>
	</div>
	<div class="line margin-top">
		<div class="x3"></div>
		<div class="x9">
			<select name="type" class="input radius-none">
				<volist name=":C('HOOKS_TYPE')" id="vo">
					<option value="{$key}" <eq name="data.type" value="$key"> selected</eq>>
						=== {$vo} ===
					</option>
				</volist>
			</select>
		</div>
	</div>
	<present name="data">
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<strong>该钩子已挂载的插件</strong>
				<span class="margin-left text-gray">
					（该钩子上已挂载的插件）
				</span>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<input type="hidden" name="addons" value="{$data.addons}" readonly>
				<empty name="data['addons']">
					暂无插件，无法排序
				<else />
					<ol>
						<volist name=":explode(',',$data['addons'])" id="addons_vo">
							<li>{$addons_vo}</li>
						</volist>
					</ol>
				</empty>
			</div>
		</div>
	</present>
	<div class="line margin-top">
		<div class="x3">
			<?php if (!empty($data)): ?>
				<input type="hidden" name="id" value="{$data.id}">
			<?php endif; ?>
		</div>
		<div class="x9">
			<button class="button radius-none bg-blue">
				<i class="icon-edit"></i>
				<present name="data">
					修改钩子
				<else />
					创建钩子
				</present>
			</button>
		</div>
	</div>
</form>
<include file="Public/Footer" />