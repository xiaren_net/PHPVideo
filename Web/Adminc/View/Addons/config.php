<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line margin-top">
		<div class="x3">
		</div>
		<div class="x9">
			<h2>[ {$data.title} ] 插件配置</h2>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<form action="{:U('saveConfig')}" class="form-auto" method"post">
		<empty name="custom_config">
			<foreach name="data['config']" item="form" key="o_key">
				<switch name="form.type">
					<case value="text">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9">
								<div>
									<input type="text" name="config[{$o_key}]" class="input radius-none" size="35" value="{$form.value}">
								</div>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="password">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9">
								<div>
									<input type="password" name="config[{$o_key}]" class="input radius-none" size="35" value="{$form.value}">
								</div>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="hidden">
						<input type="hidden" name="config[{$o_key}]" value="{$form.value}">
					</case>
					<case value="radio">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9 padding-small-top">
								<foreach name="form.options" item="opt" key="opt_k">
									<label class="padding-left">
										<input type="radio" name="config[{$o_key}]" value="{$opt_k}" <eq name="form.value" value="$opt_k"> checked</eq>>{$opt}
									</label>
								</foreach>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="checkbox">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9 padding-small-top">
								<foreach name="form.options" item="opt" key="opt_k">
									<label class="padding-left">
										<php>
											is_null($form["value"]) && $form["value"] = array();
										</php>
										<input type="checkbox" name="config[{$o_key}][]" value="{$opt_k}" <in name="opt_k" value="$form.value"> checked</in>>
										{$opt}
									</label>
								</foreach>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="select">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9">
								<select name="config[{$o_key}]" class="input radius-none">
									<foreach name="form.options" item="opt" key="opt_k">
										<option value="{$opt_k}" <eq name="form.value" value="$opt_k"> selected</eq>>=== {$opt} ===</option>
									</foreach>
								</select>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="textarea">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9">
								<textarea name="config[{$o_key}]" class="input radius-none mic-resize" cols="35" rows="5">{$form.value}</textarea>
								<present name="form['tip']">
									<div class="text-gray">
										{$form.tip}
									</div>
								</present>
							</div>
						</div>
					</case>
					<case value="picture_union">
						<div class="line margin-top">
							<div class="x3 text-right padding-small-right padding-small-top">
								{$form.title|default=''}
							</div>
							<div class="x9" id="upload_picture">
								<button type="button" class="button bg-main radius-none" id="pickfiles_{$o_key}">
									<i class="icon-image (alias)"></i>
									选择图片
								</button>
								<input type="hidden" name="config[{$o_key}]" id="cover_id_{$o_key}" value="{$form.value}"/>
								<notempty name="form['value']">
									<php> $mulimages = explode(",", $form["value"]); </php>
									<foreach name="mulimages" item="one">
										<div class="upload-pre-item" val="{$one}">
											<img src="{$one|get_cover='path'}"  ondblclick="removePicture{$o_key}(this)"/>
										</div>
									</foreach>
								</notempty>
							</div>
						</div>
						<script type="text/javascript" src="__PUBLIC__/Plupload/moxie.min.js"></script>
						<script type="text/javascript" src="__PUBLIC__/Plupload/plupload.dev.js"></script>
						<script type="text/javascript" src="__PUBLIC__/Plupload/i18n/zh_CN.js"></script>
						<script type="text/javascript">
							$(function(){
								var uploader = new plupload.Uploader({
									runtimes: 'html5,flash,html4',
									auto_start: true,
									multi_selection : false,
									browse_button: 'pickfiles_{$o_key}',
									container: 'upload_picture',
									url: "{:U('Files/UploadOnePicture',array('user_id'=> $admin_user_id))}",
									flash_swf_url: '__PUBLIC__/Plupload/Moxie.swf',
									silverlight_xap_url: '__PUBLIC__/Plupload/Moxie.xap',
									filters: {
										max_file_size: '10mb',
										mime_types: [
											{title : "图片文件", extensions : "jpg,gif,png"},
										]
									},
									init: {
										FilesAdded: function (up, files) {
											uploader.start();
										},
										UploadProgress: function (up, file) {
										},
										FileUploaded: function (up, file,response) {
											// var res = $.parseJSON(response.response);
											// $("input." + file.id).val(res.result);
										},
										Error: function (up, err) {
											if (err.code) {
												layer.msg(err.message,{
													time : 3000
												},function(){
													window.reload();
												});
											}else{
												layer.msg('图片文件上传出错，请稍后重试',{
													time : 3000
												},function(){
													window.reload();
												});
											};
										}
									}
								});
								uploader.init();
							});
						</script>
						</case>
						<case value="group">
							<div class="line margin-top">
								<div class="x12" id="upload_picture">
									<ul class="admin-tab-nav">
										<volist name="form.options" id="li">
											<li onclick="tab(this,'tab{$i}');" <eq name="i" value="1">class="admin-tab-active"</eq>><a href="javascript:void(0);">{$li.title}</a></li>
										</volist>
									</ul>
								</div>
							</div>
							<volist name="form.options" id="tab">
								<div type-data="tab" id="tab{$i}"<gt name="i" value="1"> class="mic-hidden"</gt>>
									<foreach name="tab['options']" item="tab_form" key="o_tab_key">
										<switch name="tab_form.type">
											<case value="text">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9">
														<input type="text" name="config[{$o_tab_key}]" class="input radius-none" size="35" value="{$tab_form.value}">
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="password">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9">
														<input type="password" name="config[{$o_tab_key}]" class="input radius-none" size="35" value="{$tab_form.value}">
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="hidden">
												<input type="hidden" name="config[{$o_tab_key}]" value="{$tab_form.value}">
											</case>
											<case value="radio">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9 padding-small-top">
														<foreach name="tab_form.options" item="opt" key="opt_k">
															<label class="padding-right">
																<input type="radio" name="config[{$o_tab_key}]" value="{$opt_k}" <eq name="tab_form.value" value="$opt_k"> checked</eq>>
																{$opt}
															</label>
														</foreach>
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="checkbox">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9 padding-small-top">
														<foreach name="tab_form.options" item="opt" key="opt_k">
															<label class="padding-right">
																<php> is_null($tab_form["value"]) && $tab_form["value"] = array();</php>
																<input type="checkbox" name="config[{$o_tab_key}][]" value="{$opt_k}" <in name="opt_k" value="$tab_form.value"> checked</in>>
																{$opt}
															</label>
														</foreach>
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="select">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9 padding-small-top">
														<select name="config[{$o_tab_key}]" class="input radius-none">
															<foreach name="tab_form.options" item="opt" key="opt_k">
																<option value="{$opt_k}" <eq name="tab_form.value" value="$opt_k"> selected</eq>> === {$opt} === </option>
															</foreach>
														</select>
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="textarea">
												<div class="line margin-top">
													<div class="x3 text-right padding-small-right padding-small-top">
														{$tab_form.title|default=''}
													</div>
													<div class="x9 padding-small-top">
														<textarea name="config[{$o_tab_key}]" class="input radius-none mic-resize" cols="30" rows="10">{$tab_form.value}</textarea>
														<present name="tab_form['tip']">
															<div class="text-gray">
																{$tab_form.tip}
															</div>
														</present>
													</div>
												</div>
											</case>
											<case value="picture_union">
												<div class="controls">
												<input type="file" id="upload_picture_{$o_tab_key}">
												<input type="hidden" name="config[{$o_tab_key}]" id="cover_id_{$o_tab_key}" value="{$tab_form.value}"/>
												<div class="upload-img-box">
													<notempty name="tab_form['value']">
													<php> $mulimages = explode(",", $tab_form["value"]); </php>
													<foreach name="mulimages" item="one">
														<div class="upload-pre-item" val="{$one}">
															<img src="{$one|get_cover='path'}"  ondblclick="removePicture{$o_tab_key}(this)"/>
														</div>
													</foreach>
													</notempty>
												</div>
												</div>
												<script type="text/javascript">
													//上传图片
													/* 初始化上传插件 */
													$("#upload_picture_{$o_tab_key}").uploadify({
														"height"          : 30,
														"swf"             : "__STATIC__/uploadify/uploadify.swf",
														"fileObjName"     : "download",
														"buttonText"      : "上传图片",
														"uploader"        : "{:U('File/uploadPicture',array('session_id'=>session_id()))}",
														"width"           : 120,
														'removeTimeout'   : 1,
														'fileTypeExts'    : '*.jpg; *.png; *.gif;',
														"onUploadSuccess" : uploadPicture{$o_tab_key},
														'onFallback' : function() {
												            alert('未检测到兼容版本的Flash.');
												        }
													});

													function uploadPicture{$o_tab_key}(file, data){
														var data = $.parseJSON(data);
														var src = '';
														if(data.status){
															src = data.url || '__ROOT__' + data.path
															$("#cover_id_{$o_tab_key}").parent().find('.upload-img-box').append(
																'<div class="upload-pre-item" val="' + data.id + '"><img src="__ROOT__' + src + '" ondblclick="removePicture{$o_tab_key}(this)"/></div>'
															);
															setPictureIds{$o_tab_key}();
														} else {
															updateAlert(data.info);
															setTimeout(function(){
																$('#top-alert').find('button').click();
																$(that).removeClass('disabled').prop('disabled',false);
															},1500);
														}
													}
													function removePicture{$o_tab_key}(o){
														var p = $(o).parent().parent();
														$(o).parent().remove();
														setPictureIds{$o_tab_key}();
													}
													function setPictureIds{$o_tab_key}(){
														var ids = [];
														$("#cover_id_{$o_tab_key}").parent().find('.upload-img-box').find('.upload-pre-item').each(function(){
															ids.push($(this).attr('val'));
														});
														if(ids.length > 0)
															$("#cover_id_{$o_tab_key}").val(ids.join(','));
														else
															$("#cover_id_{$o_tab_key}").val('');
													}
												</script>
											</case>
										</switch>
									</foreach>
								</div>
							</volist>
						</case>
				</switch>
		</foreach>
	<else />
		<present name="custom_config">
			{$custom_config}
		</present>
	</empty>
	<div class="line margin-top">
		<div class="x3">
			<input type="hidden" name="id" value="{:I('id')}" readonly>
		</div>
		<div class="x9 padding-small-top">
			<button type="submit" class="button radius-none bg-blue">
				<i class="icon-edit"></i>
				提交配置
			</button>
			<button type="button" onclick="GetUrl('{:U('index')}');" class="button radius-none bg-main margin-small-left">
				<i class="icon-list"></i>
				返回列表
			</button>
		</div>
	</div>
	</form>
</div>
<script type="text/javascript" charset="utf-8">
	/**
	 * [tab 切换标签]
	 * @param  {[type]} tab [description]
	 * @return {[type]}     [description]
	 */
	function tab(obj,tab){
		$(obj).parent('.admin-tab-nav').find('li').removeClass('admin-tab-active');
		$('div[type-data=tab]').addClass('mic-hidden');
		$('#'+tab).removeClass('mic-hidden');
		$(obj).addClass('admin-tab-active');
	}
</script>
<include file="Public/Footer" />