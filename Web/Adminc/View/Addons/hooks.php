<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/addhook', $admin_user_id)): ?>
				<button class="button radius-none" onclick="GetUrl('{:U('addhook')}');">
					<i class="icon-edit"></i>
					创建钩子
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/hooks', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('hooks')}');">
					<i class="icon-list"></i>
					钩子列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>序 号</td>
			<td>钩子名称</td>
			<td>钩子描述</td>
			<td>钩子类型</td>
			<td>创建时间</td>
			<td>操作项目</td>
		</tr>
		<?php foreach ($list as $key => $ret): ?>
			<tr>
				<td>{$ret.id}</td>
				<td>
					<a href="{:U('edithook', array('id'=> $ret['id']))}" class="text-yellow">{$ret.name}</a>
				</td>
				<td>{$ret.description}</td>
				<td>{$ret.type_text}</td>
				<td>{$ret.update_time|date='Y年m月d日 H:i',###}</td>
				<td>
					<a href="{:U('edithook', array('id'=> $ret['id']))}">
						<i class="icon-edit (alias)"></i>
						编 辑
					</a>
					<a href="javascript:sendGet('{:U('delhook', array('id'=> $ret['id']))}', '是否确认删除该钩子？');">
						<i class="icon-bitbucket"></i>
						删 除
					</a>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="6" align="center">
				{$_page}
			</td>
		</tr>
	</table>
</div>
<include file="Public/Footer" />