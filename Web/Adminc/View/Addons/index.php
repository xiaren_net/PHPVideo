<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/create', $admin_user_id)): ?>
				<button class="button radius-none" onclick="GetUrl('{:U('create')}');">
					<i class="icon-edit"></i>
					创建插件
				</button>
			<?php endif;?>
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					插件列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<table class="table table-hover table-condensed table-bordered">
		<tr class="bg">
			<td>名 称</td>
			<td>标 识</td>
			<td>描 述</td>
			<td>状 态</td>
			<td>作 者</td>
			<td>版 本</td>
			<td>操 作</td>
		</tr>
		<notempty name="_list">
			<?php foreach ($_list as $key => $vo): ?>
				<tr>
					<td>{$vo.title}</td>
					<td>{$vo.name}</td>
					<td>{$vo.description}</td>
					<td>{$vo.status_text}</td>
					<td>{$vo.author}</td>
					<td>{$vo.version}</td>
					<td>
						<empty name="vo['uninstall']">
							<?php
								$class	= get_addon_class($vo['name']);
								if(!class_exists($class)){
									$has_config = 0;
								}else{
									$addon = new $class();
									$has_config = count($addon->getConfig());
								}
							?>
							<?php if ($vo['has_adminlist'] == 1): ?>
								<a href="javascript:GetUrl('{:U('Addons/adminList',array('name'=> $vo['name']))}');">
									<i class="icon-database"></i>
									数据管理
								</a>
							<?php endif; ?>
							<?php if ($has_config): ?>
								<a href="{:U('config',array('id'=>$vo['id']))}">
									<i class="icon-wrench"></i>
									设 置
								</a>
							<?php endif; ?>
							<?php if ($vo['status'] >=0): ?>
								<eq name="vo['status']" value="0">
									<a href="javascript:sendGet('{:U('enable',array('id'=>$vo['id']))}', '是否确认启用该插件？');">
										<i class="icon-thumbs-o-up"></i>
										启 用
									</a>
								<else />
									<a href="javascript:sendGet('{:U('disable',array('id'=>$vo['id']))}', '是否确认禁用该插件？');">
										<i class="icon-thumbs-o-down"></i>
										禁 用
									</a>
								</eq>
							<?php endif; ?>
								{// <eq name="vo['author']" value="thinkphp">}
									<a href="javascript:sendGet('{:U('uninstall', array('id'=> $vo['id']))}', '是否确认卸载该插件？');">
										<i class="icon-external-link"></i>
										卸 载
									</a>
								{// </eq>}
							<else />
								<a href="javascript:sendGet('{:U('install', array('addon_name'=> $vo['name']))}', '是否确认安装该插件？');">
									<i class="icon-desktop"></i>
									安 装
								</a>
						</empty>
					</td>
				</tr>
			<?php endforeach; ?>
		<else/>
			<td colspan="7" align="center"> Oh! 暂时还没有插件! </td>
		</notempty>
	</table>
</div>
<include file="Public/Footer" />