<include file="Public/Header" />
<div class="container-layout margin-top">
	<div class="line border padding-top padding-bottom padding-right">
		<div class="x12 text-right">
			<?php if ($mic_auth->check(MODULE_NAME . '/' . CONTROLLER_NAME . '/index', $admin_user_id)): ?>
				<button class="button bg-main radius-none" onclick="GetUrl('{:U('index')}');">
					<i class="icon-list"></i>
					插件列表
				</button>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="container-layout margin-top">
	<form action="{:U('build')}" method="post" class="form-auto">
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>插件标识</strong>
					<span class="text-gray">（请输入插件标识）</span>
				</div>
				<div class="margin-small-top">
					<input type="text" name="info[name]" value="Example" class="input radius-none" size="45">
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>插件名称</strong>
					<span class="text-gray">（请输入插件名）</span>
				</div>
				<div class="margin-small-top">
					<input type="text"  name="info[title]" value="示列" class="input radius-none" size="45">
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>版本信息</strong>
					<span class="text-gray">（请输入插件版本）</span>
				</div>
				<div class="margin-small-top">
					<input type="text"  name="info[version]" value="0.1" class="input radius-none" size="45">
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>插件作者</strong>
					<span class="text-gray">（请输入插件作者）</span>
				</div>
				<div class="margin-small-top">
					<input type="text"  name="info[author]" value="楚羽幽" class="input radius-none" size="45">
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>插件描述</strong>
					<span class="text-gray">（请输入描述）</span>
				</div>
				<div class="margin-small-top">
					<textarea name="info[description]" class="input radius-none mic-resize" cols="45" rows="5">这是一个临时描述</textarea>
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<label class="cursor-pointer">
						<input type="checkbox" name="info[status]" value="1" checked />
						<strong>安装后是否启用</strong>
					</label>
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div>
					<label class="cursor-pointer">
						<input type="checkbox" name="has_config" value="1" />
						<strong>是否需要配置</strong>
					</label>
				</div>
				<div class="mic-hidden">
					<textarea name="config" class="input radius-none mic-resize margin-top" cols="45" rows="10">&lt;?php
return array(
	'random'=>array(// 配置在表单中的键名 ,这个会是config[random]
		'title'=>'是否开启随机:',// 表单的文字
		'type'=>'radio',		 // 表单的类型：text、password、hidden、picture_union、group、textarea、checkbox、radio、select等
		'options'=>array(		 // select 和radion、checkbox的子选项
			'1'=>'开启',		 // 值=>文字
			'0'=>'关闭',
		),
		'value'=>'1',			 // 表单的默认值
	),
);</textarea>
					<div class="margin-top">
						<input type="text" class="input radius-none" size="45" name="custom_config">
					</div>					
					<div class="margin-top text-gray">
						自定义模板,注意：自定义模板里的表单name必须为config[name]这种，获取保存后配置的值用$data.config.name
					</div>
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<label class="cursor-pointer">
					<input type="checkbox" name="has_outurl" value="1">
					<strong>是否允许外部访问</strong>
				</label>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>实现插件的钩子</strong>
				</div>
				<div class="margin-small-top">
					<select class="input" style="height:150px;width:280px;" name="hook[]" multiple required>
						<volist name="Hooks" id="vo">
							<option value="{$vo.name}" title="{$vo.description}">
								{$vo.name}
							</option>
						</volist>
					</select>
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<div class="margin-small-top">
					<strong>是否需要插件后台</strong>
				</div>
				<div class="margin-top">
					<label class="cursor-pointer">
						<input type="checkbox" name="has_adminlist" value="1" />
						勾选，扩展里已装插件后台列表会出现插件名的列表菜单，如系统的附件
					</label>
				</div>
				<div class="margin-small-top mic-hidden" id="admin_list">
					<textarea name="admin_list" class="input radius-none mic-resize" cols="45" rows="10">'model'=>'Example',		//要查的表
				'fields'=>'*',			//要查的字段
				'map'=>'',				//查询条件, 如果需要可以再插件类的构造方法里动态重置这个属性
				'order'=>'id desc',		//排序,
				'listKey'=>array( 		//这里定义的是除了id序号外的表格里字段显示的表头名
					'字段名'=>'表头显示名'
				),</textarea>
					<div class="margin-top">
						<input type="text" class="input radius-none" size="45" name="custom_adminlist">
					</div>
					<div class="margin-top text-gray">
						自定义模板,注意：自定义模板里的列表变量为$_list这种,遍历后可以用listkey可以控制表头显示,也可以完全手写，分页变量用$_page
					</div>
				</div>
			</div>
		</div>
		<div class="line margin-top">
			<div class="x3"></div>
			<div class="x9">
				<!-- <button type="button" class="button radius-none bg-main">
					预览插件
				</button> -->
				<button type="submit" class="button radius-none bg-blue">
					<i class="icon-edit"></i>
					创建插件
				</button>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(function(){
		$('input[name=has_config]').click(function(){
			$('textarea[name=config]').parent('div').toggle();
		});
		$('input[name=has_adminlist]').click(function(){
			$('#admin_list').toggle();
		});
	});
</script>
<include file="Public/Footer" />