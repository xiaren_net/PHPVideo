<?php
/**
 * 权限菜单规则模型
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Model;
use Think\Model;
class AuthRuleModel extends Model{

	// 自动完成
	protected $_auto = array(
		array('create_time', 'time', 1, 'function'),
	);

	// 自动验证
	protected $_validate = array(
		// array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
		array('name','','权限规则已经存在', 0,'unique',1),
	);
	/**
	 * [rule_add 删除规则]
	 * @return [type] [description]
	 */
	public function rule_add(){
		if ($this->create()) {
			if ($this->add()) {
				return $this->GetFile();
			}else{
				$this->error = '操作失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [rule_edit 修改规则]
	 * @return [type] [description]
	 */
	public function rule_edit(){
		if ($this->create()) {
			$status = $this->where(array('id'=> I('id', 0, 'intval')))->save();
			if ($status !== false) {
				return $this->GetFile();
			}else{
				$this->error = '操作失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [del_rule 删除权限]
	 * @return [type] [description]
	 */
	public function del_rule(){
		// 获取数据
		$id = I('id', 0, 'intval');
		$where['id|pid'] = $id;

		//删除数据
		if ($this->where($where)->delete()) {
			return $this->GetFile();
		}else{
			$this->error = '操作失败';
			return false;
		}
	}

	/**
	 * [GetFile 更新缓存规则]
	 */
	public function GetFile(){
		// 获取数据
		$data = $this->order('sort asc,id asc')->select();
		
		// 更新缓存 返回更新状态
		if(S('Auth_rule', $data)){
			return true;
		}else{
			return false;
		}
	}
}