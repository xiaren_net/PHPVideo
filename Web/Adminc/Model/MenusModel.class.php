<?php
/**
 * 管理员模型
 * 贵州MIC网络信息技术
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Model;
use Think\Model\RelationModel;
class MenusModel extends RelationModel{
    // 数据验证
	protected $_validate = array(
		// array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
		array('title','','菜单名称已经存在',0,'unique',1),
		array('url','','菜单连接已经存在',0,'unique',1),
	);
	// 自动完成
	protected $_auto = array(
		// array('url', 'strtolower', 3, 'function'),
		array('create_time', 'time', 1, 'function'),
	);

	/**
	 * [CreateMenu 创建菜单]
	 */
	public function CreateMenu(){
		if ($this->create()) {
			if ($this->add()) {
				return $this->GetFile();
			}else{
				$this->error = '创建菜单失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}
	/**
	 * [EditMenu 修改菜单]
	 */
	public function EditMenu(){
		if ($this->create()) {
			$status = $this->where(array('id'=> $_POST['id']))->save();
			if ($status !== false) {
				return $this->GetFile();
			}else{
				$this->error = '修改菜单失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [DelMenus 删除菜单]
	 */
	public function DelMenus(){
		$id = I('id', 0, 'intval');
		// 判断返回
		if ($this->where(array('id'=> $id))->delete()) {
			return $this->GetFile();
		}else{
			$this->error = '操作失败';
			return false;
		}
	}

	/**
	 * [GetFile 更新菜单缓存]
	 */
	public function GetFile(){
		$data = $this->order('create_time asc')->select();
		// 更新缓存 返回更新状态
		if(S('Menus', $data)){
			return true;
		}else{
			return false;
		}
	}
}