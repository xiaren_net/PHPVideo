<?php
/**
 * 管理组模型
 * 贵州Mic工作室
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Model;
use Think\Model;
class AuthGroupModel extends Model{
	protected $_validate = array(
		// array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
		array('title','require','请填写管理组名称', 1,'regex',3),
	);

	protected $_auto = array(
		array('addtime', 'time', 1, 'function'),
	);

	public function admin_add(){
		if ($this->create()) {
			if ($this->add()) {
				return true;
			}else{
				$this->error = '操作失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}
	public function admin_edit(){
		if ($this->create()) {
			$id = I('id', '', 'intval');
			$status = $this->where(array('id'=> $id))->save();
			if ($status !== false) {
				return true;
			}else{
				$this->error = '操作失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [mic_access 管理组授权]
	 * @return [type] [description]
	 */
	public function mic_access(){
		foreach ($_POST['access'] as $key => $evt) {
			$str .= $evt . ",";
		}
		$attr = rtrim($str, ",");
		$status = $this->where(array('id'=> I('uid', 0, 'intval')))->save(array('rules'=> $attr));
		if ($status !== false) {
			return true;
		}else{
			return false;
		}		
	}
}