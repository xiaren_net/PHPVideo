<?php
namespace Adminc\Model;
use Think\Model;
class CategoryModel extends Model{
	// 自动验证
	protected $_validate = array(
	);
	// 自动完成
	protected $_auto = array(
		array('create_time', 'time', 1, 'function'),
	);
	// 添加分类
	public function category_add(){
		if ($this->create()) {
			if ($this->add()) {
				return $this->GetFile();
			}else{
				$this->error = '添加失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	// 缓存分类
	public function GetFile(){
		// 获取数据
		$data = $this->order('sort asc,cate_id asc')->select();

		// 开始缓存
		if (S('Category', $data)) {
			return true;
		}else{
			$this->error = '视频分类缓存失败';
			return false;
		}
	}
}