<?php
/**
 * 用户模型
 * 楚羽幽《name_cyu@foxmail.com》
 */
namespace Adminc\Model;
use Think\Model;
class MemberModel extends Model{
	// 自动完成
	protected $_auto = array(
		array('user_pass', '_encrypt', 3, 'function'),
		array('create_time', 'time', 1, 'function'),
	);

	/**
	 * [member_add 添加会员]
	 * @return [type] [description]
	 */
	public function member_add(){
		if ($this->create()) {
			if ($this->add()) {
				return true;
			}else{
				$this->error = '添加失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}

	/**
	 * [member_edit 修改会员]
	 * @return [type] [description]
	 */
	public function member_edit(){
		// 判断密码是否为空
		if (empty($_POST['user_pass'])) {
			unset($_POST['user_pass']);
		}
		if (!empty($_POST['user_pass'])) {
			if ($_POST['user_pass'] != $_POST['user_pass_confirm']) {
				$this->error = '用户密码与确认密码不一致';
				return false;
			}
		}
		
		if ($this->create()) {
			$status = $this->where(array('user_id'=> $_POST['user_id']))->save();
			if ($status !== false) {
				return true;
			}else{
				$this->error = '用户编辑失败';
				return false;
			}
		}else{
			$this->error;
			return false;
		}
	}
}