<?php
/**
 * 图片管理模型
 * 贵州MIC网络信息技术
 * 楚羽幽 《name_cyu@foxmail.com》
 */
namespace Adminc\Model;
use Think\Model;
use Think\Upload;
class PictureModel extends Model{

    // 数据验证
	protected $_validate = array(
		// array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
	);

	protected $_auto = array(
		array('create_time', 'time', 1, 'function'),
	);

	/**
	 * [UploadPicture 单图片上传]
	 * @param [type] $file    [description]
	 * @param [type] $setting [description]
	 */
	public function UploadPicture($file, $setting){
		$upload 	= new Upload($setting);
		$info		= $upload->upload($file['file']);
		p($info);die;
	}
}