<?php
/**
 * [mic_rule 规则与所授权重组]
 * @param  array  $rule   [description]
 * @param  array  $access [description]
 * @return [type]         [description]
 */
function mic_rule($rule = array(), $access = ''){
    // 拆分数组
    $access = explode(',', $access);

    // 遍历数组
    foreach ($rule as $key => $evt) {
        if (is_array($access)) {
            $rule[$key]['access'] = in_array($evt['id'], $access) ? 1 : 0;
        }
    }
    return $rule;
}
/**
 * select返回的数组进行整数映射转换
 *
 * @param array $map  映射关系二维数组  array(
 *                                          '字段名1'=>array(映射关系数组),
 *                                          '字段名2'=>array(映射关系数组),
 *                                           ......
 *                                       )
 * @author 朱亚杰 <zhuyajie@topthink.net>
 * @return array
 *
 *  array(
 *      array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常')
 *      ....
 *  )
 *
 */
function mic_int_status(&$data,$map=array('status'=>array(1=>'正常',-1=>'删除',0=>'禁用',2=>'未审核',3=>'草稿'))) {
    if($data === false || $data === null ){
        return $data;
    }
    $data = (array)$data;
    foreach ($data as $key => $row){
        foreach ($map as $col=>$pair){
            if(isset($pair[$row[$col]])){
                $data[$key][$col.'_text'] = $pair[$row[$col]];
            }
        }
    }
    return $data;
}