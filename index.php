<?php
header('Content-type:text/html;charset=utf8');
// 判断PHP版本
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('PHP版本必须是 5.3.0 以上 ，贵州MIC工作室提示!');

// 框架路径
define('MIC_PATH','./Frame/');

// 调试模式
define('APP_DEBUG',true);

// 应用名称
define('APP_NAME','Web');

// 应用目录
define('APP_PATH','./Web/');

// 缓存目录
define('RUNTIME_PATH','./Cache/');

// 引用框架文件
require_once MIC_PATH . 'Action.php';