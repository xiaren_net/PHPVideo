// 全局加载
$(function(){
	// 菜单下拉
	$('.admin-top-nav').find('li').hover(function(){
		$(this).find('.admin-tow-list').removeClass('hidden');
	},function(){
		$(this).find('.admin-tow-list').addClass('hidden');
	});
});
/**
 * [sendGet 发送有提示的GET]
 * @param  {[type]} path  [description]
 * @param  {[type]} title [description]
 * @return {[type]}       [description]
 */
function sendGet(path,title){
	var path = path ? path : '';
	var title = title ? title : '';
	if (path == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	if (title == '') {
		layer.msg('请传入GET标题名称');
		return;
	};
	layer.confirm(title,function(){
		$.getJSON(path,function(msg){
			if (msg.status == 1) {
				layer.msg(msg.info,function(){
					window.location.href=msg.url;
				});
			}else{
				layer.msg(msg.info);
			};
		});
	});
}
/**
 * [GetUrl 请求目标URL]
 * @param {[type]} url [description]
 */
function GetUrl(url){
	layer_index = layer.msg('请稍等，正在加载中...',{
		icon : 16,
		time : 30 * 1000,
	});
	window.location.href=url;
}
/**
 * [send_none_Get 发送无提示GET参数]
 * @param  {[type]} path [description]
 * @return {[type]}      [description]
 */
function send_none_Get(path){
	var path = path ? path : '';
	if (path == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	$.getJSON(path,function(msg){
		if (msg.status == 1) {
			layer.msg(msg.info,function(){
				window.location.href = msg.url;
			});
		}else{
			layer.msg(msg.info);
		};
	});
}
/**
 * [url_location 提示后跳转]
 * @param  {[type]} url   [description]
 * @param  {[type]} title [description]
 * @return {[type]}       [description]
 */
function url_location(url,title){
	var url = url ? url : '';
	var title = title ? title : '';
	if (url == '') {
		layer.msg('请传入带参数GET地址');
		return;
	};
	if (title == '') {
		layer.msg('请传入GET标题名称');
		return;
	};
	layer.confirm(title,function(){
		window.location.href=url;
	});
}