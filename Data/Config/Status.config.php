<?php
return array(
	'VIDEO_STATUS'		=> array(
		0				=> '待审核',
		1				=> '已审核',
	),
	'HOOKS_TYPE'		=> array(
		1				=> '视图',
		2				=> '控制器'
	),
);